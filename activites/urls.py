"""
    Configuration des URLs de l'app « Activités »
"""

from django.conf.urls import url

from activites.views import (
    ActiviteListeView, ActiviteDetailView, ActiviteAjoutView, ActiviteChangeView,
    ActiviteSuppressionView, ActiviteRechercheView, ActiviteValideView, ActiviteDevalideView,
    ActiviteInvitationView, ActiviteInviteListeView, ActiviteInvitePresentView, ActiviteInviteAbsentView,
    ActiviteInviteRetraitView,
)

urlpatterns = [
    # Activités
    url(r'^$', ActiviteListeView.as_view(), name="accueil"),
    url(r'^rechercher/$', ActiviteRechercheView.as_view(), name="recherche"),
    url(r'^ajouter/$', ActiviteAjoutView.as_view(), name="ajout"),
    url(r'^(?P<pk>\d+)/$', ActiviteDetailView.as_view(), name="detail"),
    url(r'^(?P<pk>\d+)/supprimer/$', ActiviteSuppressionView.as_view(), name="suppression"),
    url(r'^(?P<pk>\d+)/modifier/$', ActiviteChangeView.as_view(), name="change"),
    url(r'^(?P<pk>\d+)/valider/$', ActiviteValideView.as_view(), name="validation"),
    url(r'^(?P<pk>\d+)/devalider/$', ActiviteDevalideView.as_view(), name="dévalidation"),
    url(r'^(?P<pk>\d+)/inviter/$', ActiviteInvitationView.as_view(), name="invitation"),
    url(r'^(?P<pk>\d+)/liste_invitations/$', ActiviteInviteListeView.as_view(), name="liste_invitations"),
    # Invités
    url(r'^invite/(?P<pk>\d+)/present/$', ActiviteInvitePresentView.as_view(), name="invite_present"),
    url(r'^invite/(?P<pk>\d+)/absent/$', ActiviteInviteAbsentView.as_view(), name="invite_absent"),
    url(r'^invite/(?P<pk>\d+)/retirer/$', ActiviteInviteRetraitView.as_view(), name="invite_retrait"),
]
