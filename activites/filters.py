"""
    Filtres de l'application « Activités »
"""

import django_filters as filters

from activites.models import Activite

class ActiviteRechercheFilter(filters.FilterSet):
    """
        Filtre pour rechercher un adhérent en utilisant un terme
        qui sera cherché dans le pseudo, le nom, le prénom.
    """
    class Meta:
        model = Activite
        fields = {
            'intitule' : ['icontains'],
            'debut' : ['gte', 'lte'],
            'fin' : ['gte', 'lte'],
            'liste_invitation' : ['exact'],
            'organisateur' : ['exact'],
        }

    @property
    def qs(self):
        return super().qs.order_by('id')
