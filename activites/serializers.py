"""
    Sérialiseurs de l'app « Activités »
"""

from rest_framework import serializers

from note_kfet.serializers import mixins

from activites.models import Activite, Invite

class ActiviteSerializer(mixins.DynamicFieldsMixin, serializers.ModelSerializer):
    """
        Sérialiseur pour le modèle Activite
    """
    class Meta:
        model = Activite
        fields = [
            'id', 'intitule', 'description', 'debut',
            'fin', 'liste_invitation', 'organisateur', 'valide_par',
        ]
        read_only_fields = ['id']
        default_empty = False

class InviteSerializer(serializers.ModelSerializer):
    """
        Sérialiseur pour le modèle Invite
    """
    class Meta:
        model = Invite
        fields = ['id', 'nom', 'prenom', 'naissance', 'invite_par', 'activite']
        read_only_fields = ['id']
