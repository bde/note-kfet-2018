"""
    Vues de l'API de l'application « Comptes »
"""

from django.conf.urls import url, include

from rest_framework_nested import routers

from activites.views import ActiviteViewSet, InviteViewSet

router = routers.DefaultRouter()
router.register(r'activites', ActiviteViewSet)

activite_router = routers.NestedSimpleRouter(router, r'activites', lookup="activite")
activite_router.register(r'invites', InviteViewSet, base_name="activite-invites")

urlpatterns = [
    url('^', include(router.urls)),
    url('^', include(activite_router.urls)),
]
