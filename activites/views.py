"""
    Vues de l'application « Activités »
"""

from django.urls import reverse_lazy
from django.utils import timezone
from django.shortcuts import get_object_or_404

from django.views.generic.base import RedirectView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView

from django.db.models import Q

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

from django_filters.views import FilterView

from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import detail_route

from note_kfet.utils import to_bool
from note_kfet.droits import D, Acl
from note_kfet.views.mixins import NoteMixin
from note_kfet.views.decorators import requires

from comptes.models import Adherent

from activites.models import Activite, Invite
from activites.filters import ActiviteRechercheFilter
from activites.serializers import ActiviteSerializer, InviteSerializer

################################################################################
##                      Vues concernant le site Web                           ##
################################################################################

### Vues concernant les activités

class ActiviteListeView(NoteMixin, LoginRequiredMixin, ListView):
    """
        Page d'accueil de l'app « Activités »

        Liste les activités à venir.
    """
    app_label = "activités"

    model = Activite
    ordering = ['debut', 'fin']
    paginate_by = 10

    template_name = "activite_liste.html"

    def get_queryset(self):
        qs = super().get_queryset().filter(fin__gte=timezone.now())
        if not self.request.user.has_perm("activites.activite_gerer", Acl.BASIQUE):
            return qs.filter(Q(organisateur=self.request.user) | Q(valide_par__isnull=False))
        else:
            return qs

class ActiviteDetailView(NoteMixin, LoginRequiredMixin, DetailView):
    """
        Vue pour accéder au détail d'une activité
    """
    app_label = "activités"

    model = Activite

    template_name = "activite_detail.html"

    def get_queryset(self):
        qs = super().get_queryset().filter(fin__gte=timezone.now())
        if not self.request.user.has_perm("activites.activite_gerer", Acl.BASIQUE):
            return qs.filter(Q(organisateur=self.request.user) | Q(valide_par__isnull=False))
        else:
            return qs

class ActiviteAjoutView(NoteMixin, PermissionRequiredMixin, LoginRequiredMixin, CreateView):
    """
        Vue permettant d'ajouter une nouvelle activité
    """
    app_label = "activités"

    permission_required = D("activites.activite_ajouter", Acl.LIMITE)

    model = Activite
    fields = [
        'intitule', 'description', 'debut',
        'fin', 'liste_invitation', 'organisateur',
    ]

    template_name = "activite_ajout.html"

    def get_form(self, *args, **kwargs):
        form = super().get_form(*args, **kwargs)
        user = self.request.user
        qs = Adherent.objects.all().order_by('id')
        if user.has_perm("activites.activite_ajouter", Acl.TOTAL):
            form.fields['organisateur'].queryset = qs
        else:
            form.fields['organisateur'].queryset = qs.filter(pk=user.id)
        return form

    def get_success_url(self):
        return reverse_lazy("activités:detail", kwargs={'pk' : self.object.id})

class ActiviteChangeView(NoteMixin, PermissionRequiredMixin, LoginRequiredMixin, UpdateView):
    """
        Vue permettant l'édition d'une activité
    """
    app_label = "activités"

    permission_required = D("activites.activite_modifier", Acl.LIMITE)

    model = Activite
    fields = [
        'intitule', 'description', 'debut',
        'fin', 'liste_invitation', 'organisateur',
    ]

    template_name = "activite_change.html"

    def get_form(self, *args, **kwargs):
        form = super().get_form(*args, **kwargs)
        user = self.request.user
        qs = Adherent.objects.all().order_by('id')
        if user.has_perm("activites.activite_modifier", Acl.TOTAL):
            form.fields['organisateur'].queryset = qs
        else:
            form.fields['organisateur'].queryset = qs.filter(pk=user.id)
        return form

    def get_success_url(self):
        return reverse_lazy("activités:detail", kwargs=self.kwargs)

class ActiviteSuppressionView(PermissionRequiredMixin, LoginRequiredMixin, RedirectView):
    """
        Vue permettant de supprimer une activité
    """
    permanent = False
    pattern_name = "activités:accueil"

    permission_required = D("activites.activite_modifier", Acl.LIMITE)

    def get_redirect_url(self, *args, **kwargs):
        """
            Supprime l'activité avant de rediriger l'utilisateur vers
            la liste des activités.
        """
        activite = get_object_or_404(Activite, pk=kwargs.pop('pk'))
        user = self.request.user
        if user.has_perm("activites.activite_modifier", Acl.TOTAL):
            activite.delete()
        elif activite.organisateur.id == user.id:
            activite.delete()
        else:
            messages.error(self.request, "Vous ne pouvez pas supprimer cette activité")
        return super().get_redirect_url(*args, **kwargs)

class ActiviteRechercheView(NoteMixin, PermissionRequiredMixin, LoginRequiredMixin, FilterView):
    """
        Vue pour la recherche avancée dans les comptes.
    """
    app_label = "activités"

    permission_required = D("activites.activite_gerer", Acl.LIMITE)

    template_name = "activite_recherche.html"
    filterset_class = ActiviteRechercheFilter

    def get_filterset_kwargs(self, filterset_class):
        kwargs = super().get_filterset_kwargs(filterset_class)
        kwargs.update({
            'request' : self.request,
        })
        return kwargs

class ActiviteValideView(PermissionRequiredMixin, LoginRequiredMixin, RedirectView):
    """
        Vue pour valider une activité
    """
    permanent = False
    pattern_name = "activités:accueil"

    permission_required = D("activites.activite_gerer", Acl.ETENDU)

    def get_redirect_url(self, *args, **kwargs):
        """
            Valide l'activité avant de rediriger vers la liste des activités
        """
        activite = get_object_or_404(Activite, pk=kwargs.pop('pk'))
        if activite.valide_par is not None:
            messages.warning(self.request, "Cette activité a déjà été validée")
        activite.valide_par = activite.valide_par or self.request.user
        activite.save()
        return super().get_redirect_url(*args, **kwargs)

class ActiviteDevalideView(PermissionRequiredMixin, LoginRequiredMixin, RedirectView):
    """
        Vue pour dévalider une activité
    """
    permanent = False
    pattern_name = "activités:accueil"

    permission_required = D("activites.activite_gerer", Acl.ETENDU)

    def get_redirect_url(self, *args, **kwargs):
        """
            Dévalide l'activité avant de rediriger vers la liste des activités
        """
        activite = get_object_or_404(Activite, pk=kwargs.pop('pk'))
        if activite.valide_par is None:
            messages.warning(self.request, "Cette activité est déjà dévalidée")
        activite.valide_par = None
        activite.save()
        return super().get_redirect_url(*args, **kwargs)

class ActiviteInvitationView(NoteMixin, PermissionRequiredMixin, LoginRequiredMixin, CreateView):
    """
        Vue permettant d'inviter une personne à une activité
    """
    app_label = "activités"

    permission_required = D("activites.invite_inviter", Acl.LIMITE)

    model = Invite
    fields = ['prenom', 'nom', 'naissance', 'activite', 'invite_par']

    template_name = "activite_invitation.html"

    def get_form(self, form_class=None):
        form = super().get_form(form_class=form_class)
        user = self.request.user
        if user.has_perm("activites.invite_inviter", Acl.TOTAL):
            form.fields['invite_par'].queryset = Adherent.objects.filter(supprime=False).order_by('id')
        else:
            form.fields['invite_par'].queryset = Adherent.objects.filter(pk=user.id)

        if user.has_perm("activites.activite_gerer", Acl.ETENDU):
            form.fields['activite'].queryset = Activite.objects.all()
        else:
            form.fields['activite'].queryset = Activite.objects.filter(organisateur=user)

        return form

    def get_initial(self):
        initial = super().get_initial()
        initial.update({
            'activite' : get_object_or_404(Activite, pk=self.kwargs['pk']),
        })
        return initial

    def get_context_data(self, **kwargs):
        """
            Rajoute l'activité courante au contexte de gabarit
        """
        context = super().get_context_data(**kwargs)
        context.update({
            'activite' : get_object_or_404(Activite, pk=self.kwargs['pk']),
        })
        return context

    def get_success_url(self, **kwargs):
        return reverse_lazy("activités:accueil")

### Vues concernant les invités

class ActiviteInviteListeView(NoteMixin, PermissionRequiredMixin, LoginRequiredMixin, ListView):
    """
        Vue pour renvoyer la liste des invités à une activité donnée
    """
    app_label = "activités"

    permission_required = D("activites.activite_gerer", Acl.ETENDU)

    model = Invite

    template_name = "activite_liste_invitations.html"

    def get_queryset(self):
        return super().get_queryset().filter(activite=get_object_or_404(Activite, pk=self.kwargs['pk']))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'activite' : get_object_or_404(Activite, pk=self.kwargs['pk']),
        })
        return context

class ActiviteInvitePresentView(PermissionRequiredMixin, LoginRequiredMixin, RedirectView):
    """
        Vue pour noter un invité comme présent.
    """
    permanent = False
    pattern_name = "activités:liste_invitations"

    permission_required = D("activites.activite_gerer", Acl.ETENDU)

    def get_redirect_url(self, *args, **kwargs):
        """
            Note l'invité comme présent avant de rediriger vers la liste d'invitation
            de l'activité.
        """
        invite = get_object_or_404(Invite, pk=kwargs.pop('pk'))
        if invite.present is True:
            messages.warning(self.request, "Cet invité est déjà noté comme présent")
        elif invite.present is False:
            messages.warning(self.request, "Cet invité était noté comme absent")
        invite.present = True
        invite.save()
        kwargs.update(pk=invite.activite.id)
        return super().get_redirect_url(*args, **kwargs)

class ActiviteInviteAbsentView(PermissionRequiredMixin, LoginRequiredMixin, RedirectView):
    """
        Vue pour noter un invité comme absent.
    """
    permanent = False
    pattern_name = "activités:liste_invitations"

    permission_required = D("activites.activite_gerer", Acl.ETENDU)

    def get_redirect_url(self, *args, **kwargs):
        """
            Note l'invité comme présent avant de rediriger vers la liste d'invitation
            de l'activité.
        """
        invite = get_object_or_404(Invite, pk=kwargs.pop('pk'))
        if invite.present is True:
            messages.warning(self.request, "Cet invité était noté comme présent")
        elif invite.present is False:
            messages.warning(self.request, "Cet invité est déjà noté comme absent")
        invite.present = False
        invite.save()
        kwargs.update(pk=invite.activite.id)
        return super().get_redirect_url(*args, **kwargs)

class ActiviteInviteRetraitView(PermissionRequiredMixin, LoginRequiredMixin, RedirectView):
    """
        Vue pour retirer un invité de la liste d'invitation.
    """
    permanent = False
    pattern_name = "activités:liste_invitations"

    permission_required = D("activites.activite_gerer", Acl.ETENDU)

    def get_redirect_url(self, *args, **kwargs):
        """
            Note l'invité comme présent avant de rediriger vers la liste d'invitation
            de l'activité.
        """
        invite = get_object_or_404(Invite, pk=kwargs.pop('pk'))
        if invite.present is True:
            messages.warning(self.request, "Cet invité était noté comme présent")
        elif invite.present is False:
            messages.warning(self.request, "Cet invité est déjà noté comme absent")
        kwargs.update(pk=invite.activite.id)
        invite.delete()
        return super().get_redirect_url(*args, **kwargs)

################################################################################
##                          Vues concernant l'API                             ##
################################################################################

class ActiviteViewSet(viewsets.GenericViewSet):
    """
        Un ensemble de vues pour gérer les activités
    """
    queryset = Activite.objects.all()
    serializer_class = ActiviteSerializer

    def list(self, request):
        """
            Renvoie la liste des activités enregistrées.
            Le paramètre 'all' peut prendre deux valeurs : 'false' pour
            afficher seulement les activités en cours ou à venir, ou 'true' pour
            afficher toutes les activités. La valeur par défaut est 'false'.
            Les données doivent:
                -   être envoyées via un requête GET
                -   contenir dans la querystring le paramètre 'all'
        """
        show_all = to_bool(request.query_params.get('all', False))
        if show_all is None:
            return Response(
                {"detail" : "Paramètre 'all' incorrect"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        lookup_args = {'fin__gte' : timezone.now()} if show_all else {}
        qs = self.get_queryset().filter(**lookup_args)
        fields = ['id', 'intitule', 'description', 'debut', 'fin', 'liste_invitation']
        serializer = self.get_serializer(qs, many=True, fields=fields)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def retrieve(self, request, pk=None):
        """
            Renvoie les détails sur une activité donnée.
            Les données doivent:
                -   être envoyées via une requête GET
        """
        fields = ['id', 'intitule', 'description', 'debut', 'fin', 'liste_invitation']
        if self.request.user.has_perm("activites.activite_gerer", Acl.BASIQUE):
            fields += ['organisateur', 'valide_par']

        serializer = self.get_serializer(self.get_object(), fields=fields)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @requires("activites.activite_ajouter")
    def create(self, request):
        """
            Ajoute une nouvelle activité.
            Les données doivent:
                -   être envoyées via une requête POST
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        if (not request.user.has_perm("activites.activite_ajouter", Acl.ETENDU)
                and serializer.validated_data['organisateur'] != request.user):
            return Response(
                {
                    "detail" : "Vous ne pouvez pas indiquer un autre organisateur que vous-même",
                },
                status=status.HTTP_403_FORBIDDEN
            )

        activite = serializer.save()
        return Response({"id" : activite.id}, status=status.HTTP_201_CREATED)

    @requires("activites.activite_modifier")
    def partial_update(self, request, pk=None):
        """
            Modifie une activité donnée. Il n'est pas possible de valider ou dévalider
            une proposition d'activité avec cette vue de l'API.
            Les données doivent :
                -   être envoyées via une requête PATCH
        """
        fields = ['intitule', 'description', 'debut', 'fin', 'liste_invitation', 'organisateur']
        activite = self.get_object()
        if (activite.organisateur != request.user
                and not request.user.has_perm("activites.activite_modifier", Acl.ETENDU)):
            return Response(
                {
                    "detail" : "Vous ne pouvez pas modifier cette activité",
                },
                status=status.HTTP_403_FORBIDDEN,
            )
        serializer = self.get_serializer(activite, data=request.data, partial=True, fields=fields)
        serializer.is_valid(raise_exception=True)
        if ('organisateur' in serializer.validated_data
                and not request.user.has_perm("activites.activite_modifier", Acl.ETENDU)):
            return Response(
                {"detail" : "Vous ne pouvez pas changer l'organisateur d'une activité"},
                status=status.HTTP_403_FORBIDDEN,
            )

        serializer.save()
        return Response({}, status=status.HTTP_200_OK)

    @detail_route(methods=['patch'])
    @requires("activites.activite_gerer", Acl.ETENDU)
    def valide(self, request, pk=None):
        """
            Valide ou dévalide une activité.
            Les données doivent :
                -   être envoyées via une requête PATCH
                -   contenir un unique paramètre 'valide' pouvant valoir 'true' ou 'false'
        """
        valide = to_bool(request.data.get('valide', None))
        if valide is None:
            return Response(
                {"detail" : "Paramètre 'valide' manquant"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        activite = self.get_object()
        serializer = self.get_serializer(
            activite, data=request.data, partial=True, fields=['valide'],
        )
        serializer.save()
        return Response({}, status=status.HTTP_200_OK)

    @requires("activites.activite_modifier")
    def destroy(self, request, pk=None):
        """
            Supprime une activité.
            Les données doivent :
                -   être envoyées via une requête DELETE
        """
        activite = self.get_object()
        if (activite.organisateur != request.user
                and not request.user.has_perm("activites.activite_modifier", Acl.ETENDU)):
            return Response(
                {
                    "detail" : "Vous n'avez pas l'accréditation necéssaire pour "
                               "supprimer cette activité"
                },
                status=status.HTTP_403_FORBIDDEN,
            )

        activite.delete()
        return Response({}, status=status.HTTP_204_NO_CONTENT)


class InviteViewSet(viewsets.GenericViewSet):
    """
        Un ensemble du vues pour gérer les invités
    """
    queryset = Invite.objects.all()
    serializer_class = InviteSerializer

    def get_queryset(self):
        lookup_args = {
            'activite' : self.kwargs['activite_pk'],
            'activite__liste_invitation' : True,
        }
        return super().get_queryset().filter(**lookup_args)

    @requires("activites.activite_gerer", Acl.TOTAL)
    def list(self, request, activite_pk=None):
        """
            Renvoie la liste des invités à une activité donnée.
            Les données doivent :
                -   être envoyées via une requête GET
        """
        serializer = self.get_serializer(self.get_queryset(), many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @requires("activites.activite_gerer", Acl.TOTAL)
    def retrieve(self, request, pk=None, activite_pk=None):
        """
            Renvoie les informations concernant un invité donné.
            Les données doivent :
                -   être envoyées via une requête GET
        """
        invite = self.get_object()
        serializer = self.get_serializer(invite)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @requires("activites.invite_inviter", Acl.LIMITE)
    def create(self, request, activite_pk=None):
        """
            Ajoute un invité à l'activité courante
            Les données doivent :
                -   être envoyées via une requête POST
        """
        activite = Activite.objects.get(pk=activite_pk)
        if not activite.liste_invitation:
            # Pas de liste d'invités pour cette activité
            return Response(
                {"detail" : "Il n'y a pas de liste d'invités pour cette activité"},
                status=status.HTTP_409_CONFLICT,
            )

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        if (serializer.validated_data['invite_par'] != request.user
                and not request.user.has_perm("activites.invite_inviter", Acl.TOTAL)):
            return Response(
                {"detail" : "Vous ne pouvez inviter une personne qu'en vôtre nom"},
                status=status.HTTP_403_FORBIDDEN,
            )

        serializer.save()
        return Response({}, status=status.HTTP_201_CREATED)

    @requires("activites.invite_supprimer")
    def destroy(self, request, pk=None, activite_pk=None):
        """
            Supprime un invité à une activité donnée.
            Les données doivent :
                -   être envoyées via une requête DELETE
        """
        invite = self.get_object()
        if (invite.invite_par != request.user
                and not request.user.has_perm("activites.invite_supprimer", Acl.TOTAL)):
            return Response(
                {"detail" : "Vous ne pouvez pas supprimer une invitation d'un autre adhérent"},
                status=status.HTTP_403_FORBIDDEN,
            )

        invite.delete()
        return Response({}, status=status.HTTP_204_NO_CONTENT)
