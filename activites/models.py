"""
    Modèles de l'application Activités
"""

from django.db import models
from django.utils import timezone
from django.core.exceptions import ValidationError

class Activite(models.Model):
    """
        Une activité proposée par un adhérent ou un groupe d'adhérents.

        -   intitule            [varchar(255)]  :   Intitulé de l'activité
        -   description         [varchar]       :   Description de l'activité
        -   debut               [tstz]          :   Date de début de l'activité
        -   fin                 [tstz]          :   Date de fin de l'activité
        -   liste_invitation    [boolean]       :   Y a-t-il une liste d'invitation ?
        -   organisateur        [int*]          :   Organisateur de l'activité
        -   valide_par          [int*]          :   Personne ayant validé l'activité
    """
    intitule = models.CharField('intitulé', max_length=255, null=False, blank=False)
    description = models.TextField(null=False, blank=True)
    debut = models.DateTimeField('début', null=False, blank=False)
    fin = models.DateTimeField(null=False, blank=False)
    liste_invitation = models.BooleanField("sur liste d'invitation ?", default=False)
    organisateur = models.ForeignKey(
        'comptes.Adherent', on_delete=models.CASCADE,
        null=False, blank=False, related_name="activites_organisees",
    )
    valide_par = models.ForeignKey(
        'comptes.Adherent', on_delete=models.CASCADE, verbose_name="validé par",
        null=True, blank=True, related_name="activites_validees",
    )

    class Meta:
        unique_together = [
            ('intitule', 'debut', 'fin'),
        ]
        default_permissions = []
        permissions = [
            ('activite_ajouter', "Ajouter/supprimer une activité"),
            ('activite_modifier', "Modifier une activité"),
            ('activite_gerer', "Gérer les activités"),
        ]

    def __str__(self):
        return "Activité n°%d : %s" % (self.id, self.intitule)

    @property
    def upcoming(self):
        """
            Indique si l'évènement est à venir ou non.
        """
        return self.debut > timezone.now()

    @property
    def in_progress(self):
        """
            Indique si l'activité est en cours.
        """
        return self.debut <= timezone.now() <= self.fin

    def clean(self):
        """
            Procède à quelques vérifications sur le modèle.

            -   Cohérence entre date de début et date de fin
        """
        if self.debut > self.fin:
            raise ValidationError("Les dates de début et de fin de l'activité sont incorrectes")

    def save(self, *args, **kwargs):
        """
            Méthode save modifiée pour vérifier les contraintes sur le modèle.
        """
        self.full_clean(exclude=['id'] if self.id is None else [])
        super().save(*args, **kwargs)


class Invite(models.Model):
    """
        Un invité à une activité.

        -   nom         [varchar(255)]  :   Nom de l'invité
        -   prenom      [varchar(255)]  :   Prénom de l'invité
        -   naissance   [tstz]          :   Date de naissance
        -   activite    [int*]          :   Activité à laquelle l'invité participe
        -   invite_par  [int*]          :   Adhérent invitant cette personne
        -   present     [bool]          :   Invité présent à l'activité ?
    """
    nom = models.CharField(max_length=255, null=False, blank=False)
    prenom = models.CharField('prénom', max_length=255, null=False, blank=False)
    naissance = models.DateField('date de naissance', null=False, blank=False)
    activite = models.ForeignKey(
        'activites.Activite', on_delete=models.CASCADE, verbose_name="activité",
        null=False, blank=False, related_name="invites",
    )
    invite_par = models.ForeignKey(
        'comptes.Adherent', on_delete=models.CASCADE, verbose_name="invité par",
        null=False, blank=False, related_name="invites",
    )
    present = models.NullBooleanField("présent", default=None)

    class Meta:
        unique_together = [
            # On invite une personne une seule fois à une activité donnée
            ('nom', 'prenom', 'naissance', 'activite'),
        ]
        default_permissions = []
        permissions = [
            ('invite_inviter', "Inviter une personne à une activité"),
            ('invite_supprimer', "Supprimer un invité"),
        ]

    def __str__(self):
        return "Invité #%d : %s %s (%s)" % (self.id, self.prenom, self.nom, self.naissance)

    def clean(self):
        """
            Procède à quelques vérifications sur le modèle.

            -   Limite le nombre maximal d'invité par adhérent par activité à 3
            -   Limite le nombre de fois qu'une personne peut être invitée à 5
                sur une année glissante
        """
        if not self.activite.liste_invitation:
            raise ValidationError(
                "Il n'y a pas de liste d'invitation pour cette activité"
            )

        if self.__class__.objects.filter(invite_par=self.invite_par, activite=self.activite).count() >= 3:
            raise ValidationError(
                "Vous ne pouvez pas inviter plus de 3 personnes à une activité donnée"
            )

        if self.__class__.objects.filter(nom=self.nom, prenom=self.prenom, activite__debut__gte=timezone.now()).count() >= 5:
            raise ValidationError(
                "Vous ne pouvez pas inviter une même personne plus de 5 fois sur une année glissante"
            )

    def save(self, *args, **kwargs):
        """
            Méthode save modifiée pour vérifier les contraintes sur le modèle.
        """
        self.full_clean(exclude=['id'] if self.id is None else [])
        super().save(*args, **kwargs)
