NOTEREPO="/var/www/note-kfet"
PGVERSION="9.6"

echo "Hostname: "
read HOSTNAME





echo "Installing apt packages ... "
sudo apt install virtualenv postgresql-$PGVERSION postgresql-plpython3-$PGVERSION nginx uwsgi uwsgi-plugin-python3
echo "DONE\n"

echo "Creating user note ... "
sudo adduser --system --group --home $NOTEREPO --no-create-home --disabled-password --disabled-login note
sudo chown -R note:note $NOTEREPO
sudo chmod -R g+w $NOTEREPO
sudo find $NOTEREPO -type d -exec chmod g+s {} \;
echo "DONE\n"

echo "Virtualenv setup ... "
sudo -u note virtualenv -p python3 $NOTEREPO/.env
sudo -u note sh -c "$NOTEREPO/.env/bin/pip3 install django psycopg2 jinja2 django-filter djangorestframework drf-nested-routers pillow"
echo "DONE\n"

echo "Creating database ... "
sudo -u postgres psql -c "CREATE USER note;"
sudo -u postgres psql -c "CREATE DATABASE note_kfet OWNER note;"
sudo sed -i -e "$a local\tnote_kfet\tnote\ttrust" /etc/postgresql/9.6/main/pg_hba.conf
sudo systemctl reload postgresql
echo "DONE\n"

echo "secrets.py and settings.py setup ..."
sudo -u note cp $NOTEREPO/note_kfet/settings.py.example $NOTEREPO/note_kfet/settings.py
sudo -u note cp $NOTEREPO/note_kfet/secrets.py.example $NOTEREPO/note_kfet/secrets.py
SECRET_KEY=$(python3 -c 'import random; result = "".join([random.choice("abcdefghijklmnopqrstuvwxyz0123456789") for i in range(50)]); print(result)')
sudo -u note sed -i -e "s/'\s*Mettez la clef secrete de votre projet ici\s*'/'$SECRET_KEY'/" $NOTEREPO/note_kfet/secrets.py
sudo -u note sed -i -e "s/SITE_DOMAIN = None/SITE_DOMAIN = '$HOSTNAME'/" $NOTEREPO/note_kfet/settings.py
echo "DONE\n"

echo "Database setup ... "
sudo -u note sh -c "$NOTEREPO/.env/bin/python3 $NOTEREPO/manage.py migrate"
sudo -u postgres sh -c "$NOTEREPO/.env/bin/python3 $NOTEREPO/note_kfet/triggers.py"
sudo -u note sh -c "$NOTEREPO/.env/bin/python3 $NOTEREPO/note_kfet/db_initial.py"
echo "DONE\n"

echo "Uwsgi setup ... "
sudo cp $NOTEREPO/note_kfet/note.uwsgi /etc/uwsgi/apps-available/note.ini
sudo ln -s ../apps-available/note.ini /etc/uwsgi/apps-enabled/note.ini
sudo service uwsgi restart
echo "DONE\n"

echo "Nginx setup ... " 
sudo cp $NOTEREPO/note_kfet/note.nginx /etc/nginx/sites-available/note
sudo ln -s ../sites-available/note /etc/nginx/sites-enabled/note
sudo service nginx restart
echo "DONE\n"
