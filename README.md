Note Kfet
=========

Paquets requis
--------------

Les paquets peuvent être récupérés via APT ou PIP.

*   python3 (>= 3.5)
*   python3-django (>= 2.1)
*   python3-psycopg2 (>= 2.6)
*   python3-jinja2 (>= 2.10)
*   python3-django-filters (>= 2.0.0)
*   python3-djangorestframework (>= 3.8.2)
*   drf-nested-routers (>= 0.90.2)
*   python3-pil (>= 4.0.0)
*   postgresql (>= 9.6)
*   postgresql-plpython3 (>= 9.6)
*   nginx (>= 1.10)
*   uwsgi (>= 2.0.14)
*   uwsgi-plugin-python3 (>= 2.0.14)

Installation
------------

1.  Clonage du dépôt

    Cloner le dépôt à l'endroit souhaité (par exemple `/var/www/note-kfet/`)

        $ git clone https://gitlab.crans.org/dely/note-kfet.git

    Le dossier contenant le projet sera noté dans la suite `$NOTEREPO`.
    On génère ensuite un utilisateur spécial pour le projet.

        $ sudo adduser --system --group --home $NOTEREPO --no-create-home --disabled-password --disabled-login note
        $ sudo chown -R note:note $NOTEREPO
        $ sudo chmod -R g+w $NOTEREPO
        $ sudo find $NOTEREPO -type d -exec chmod g+s {} \;

    Pensez à vous rajouter au groupe 'note' et à changer votre umask lorsque vous travaillez
    sur le projet.

    Générez une clé secrète pour votre projet et mettez là dans note_kfet/secrets.py.
    Ajoutez également le domaine depuis lequel vous allez servir le site dans SITE_DOMAIN.

2.  Mise en place de la base de données

    Après avoir cloné le dépôt à l'endroit souhaité, il faut mettre en place la BDD :

        $ sudo -u postgres psql
        postgres=# CREATE USER note;
        postgres=# CREATE DATABASE note_kfet OWNER note;
        postgres=# \c note_kfet
        $ sudo service postgresql reload

    Il faut ensuite ajouter les lignes adéquates dans le fichier `pg_hba.conf` en fonction de votre
    infrastructure. Consultez la [doc PostgreSQL](http://docs.postgresql.fr/9.6/client-authentication.html#auth-pg-hba-conf)
    pour trouver la marche à suivre.

    Vous pouvez ensuite installer le schéma de la base de données, mettre en place les triggers et y insérer vos données.

        $ sudo -u note $NOTEREPO/manage.py migrate
        $ sudo -u postgres $NOTEREPO/note_kfet/triggers.py
        $ sudo -u note $NOTEREPO/note_kfet/db_initial.py

3.  Configuration de uWSGI + Nginx

    Une fois la base de données mise en place, il faut mettre en place le serveur Web. Plus d'informations
    sont disponbibles [ici](http://uwsgi-docs.readthedocs.io/en/latest/tutorials/Django_and_nginx.html?highlight=Django).

    Pour uWSGI :

        $ sudo cp $NOTEREPO/note_kfet/note.uwsgi /etc/uwsgi/apps-available/note.ini
        $ sudo ln -s /etc/uwsgi/apps-available/note.ini /etc/uwsgi/apps-enabled/note.ini
        $ sudo service uwsgi reload

    Pour Nginx :

        $ sudo cp $NOTEREPO/note_kfet/note.nginx /etc/nginx/sites-available/note
        $ sudo ln -s /etc/nginx/sites-available/note /etc/uwsgi/sites-enabled/note
        $ sudo service nginx reload

    Vous pouvez ensuite essayer d'accéder à la note depuis votre navigateur.

API RESTful
-----------

La Note Kfet possède une API RESTful, utilisant Django REST Framework. Après l'envoi d'une requête, le serveur
renverra un code HTTP en fonction du résultat de la requête. Le sens des codes HTTP reçus est résumé dans le tableau
ci-dessous :

|            Code HTTP           |         Signification       |
| ------------------------------ | --------------------------- |
| 200 (*OK*)                     | Opération effectuée         |
| 201 (*Created*)                | L'objet a bien été créé     |
| 204 (*No Content*)             | L'objet a été bien supprimé |
| 304 (*Not Modified*)           | L'objet n'a pas été modifié |
| 400 (*Bad Request*)            | Données erronnées           |
| 401 (*Unauthorized*)           | Authentification requise    |
| 403 (*Forbidden*)              | Autorisation requise        |
| 404 (*Not Found*)              | Objet introuvable           |
| 405 (*Method Not Allowed*)     | Méthode non autorisée       |
| 422 (*Unprocessable Entity*)   | Données illisibles          |
| 429 (*Too Many Requests*)      | Débit des requêtes anormal  |
| 500 (*Internal Server Error*)  | Erreur interne              |

En cas d'erreur, le serveur retournera également un message détaillant indiquant plus précisément
les raisons de l'erreur.
