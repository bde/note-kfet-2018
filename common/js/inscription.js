/* Fonctions pour afficher/cacher des éléments de formulaires */
function cacher(champs) {
    for (let champ of champs) {
        $('#id_' + champ).parent('.form-group').addClass('hidden-xs-up');
    }
}

function afficher(champs) {
    for (let champ of champs) {
        $('#id_' + champ).parent('.form-group').removeClass('hidden-xs-up');
    }
}

function adapter_form_pour_type() {
    var new_type = $(this).val();
    var champs = ['prenom', 'sexe', 'section', 'remunere', 'remarque', 'pbsante'];

    if (new_type != 'personne') {
        cacher(champs);
        $('#id_prenom').attr("required", false);
        $('#id_section').attr("required", false);
        if (new_type != 'débit') {
            $('[for=id_nom]').text($(this).find('option:selected').text());
        } else {
            cacher(['nom']);
            $('#id_nom').attr("required", false);
        }
    } else {
        afficher(champs);
        afficher(['nom']);
        $('#id_nom').attr("required", true);
        $('#id_prenom').attr("required", true);
        $('#id_section').attr("required", true);
        $('[for=id_nom]').text('Nom');
    }
}

/* Lorsque le type de compte à créer change, on masque certains champs */
$('#id_type').change(adapter_form_pour_type);
$('#id_type').each(adapter_form_pour_type);
