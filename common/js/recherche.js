/* Callbacks pour consulter le détail d'un résultat de recherche via la page de recherche */
function bindDetailPages(data_attribute, template_url) {
    function redirectToDetailPage (index) {
        var attr_id = $(this).data(data_attribute);
        $(this).click(function (){window.location = template_url.format(attr_id);});
    }
    $.each($('#tableResultats tbody tr'), redirectToDetailPage);
}

/* Générateurs de callbacks pour mettre à jour la table des résultats d'une page de recherche */
function renewResultTable(column_generators, data_attribute, template_url) {
    return function (data) {
        clearInputState('#inputRechRapide');
        $('#tableResultats table tbody tr').remove();
        var table_resultats = $('#tableResultats table tbody');
        var new_lign = null;
        var new_case = null;
        if (data.length === 0) {
            invalidInput('#inputRechRapide');
            new_lign = $(document.createElement("tr")).addClass('text-center');
            new_case = $(document.createElement("td")).text("Aucun résultat").attr('colspan', column_generators.length);
            new_case.addClass('lead');
            new_lign.append(new_case);
            table_resultats.append(new_lign);
        } else {
            validInput('#inputRechRapide');
            for (const result of data) {
                new_lign = $(document.createElement("tr"));
                new_lign.attr("data-{0}".format(data_attribute), result.id);
                for (const column of column_generators) {
                    new_case = $(document.createElement("td")).text(column(result));
                    new_lign.append(new_case);
                }
                table_resultats.append(new_lign);
            }
            bindDetailPages(data_attribute, template_url);
        }
    }
}

/* Générateur de callbacks pour indiquer une recherche infructueuse */
function showNoResults(colspan) {
    return function (jqxhr) {
        invalidInput("#inputRechRapide")
        $('#tableResultats table tbody tr').remove();
        var table_resultats = $('#tableResultats table tbody');
        var new_lign = $(document.createElement("tr")).addClass('text-center');
        var new_case = $(document.createElement("td")).text("Aucun résultat").attr('colspan', colspan);
        new_lign.append(new_case);
        table_resultats.append(new_lign);
        if (jqxhr.status !== 404) {
            display_message(jqxhr.responseJSON.detail, "error");
        }
    }
}

/* Fonction permettant l'extraction de valeurs associées à une même clé dans liste de dictionnaire */
function extractFromDictList(dictlist, key) {
    var values = [];
    for (const dict of dictlist){if (dict !== null) {values.push(dict[key]);}}
    if (values.length == 0) {
        return [""];
    } else {
        return values;
    }
}
