/* Fonction wrappant les appels à la lib AJAX de jQuery
 *
 * Place automatiquement le CSRF-Token lorsque des requêtes différentes de GET/HEAD/OPTIONS/TRACE sont effectuées
 * Transforme les données à envoyer en JSON
 * Décode les données reçues du serveur */
function xhr(url, method, data, success, fail) {
    "use strict";
    success = (success === undefined) ? doNothing : success;
    fail = (fail === undefined) ? doNothing : fail;
    var safe_methods = ["GET", "HEAD", "OPTIONS", "TRACE"];
    var xhr_settings = {
        'type' : method.toLocaleUpperCase(),
        'url' : url,
        'contentType' : 'application/json',
        'accepts' : {'json' : 'application/json'},
        'dataType' : 'json',
        'data' : (typeof data === "string") ? data : JSON.stringify(data),
    };
    if (!safe_methods.includes(method.toLocaleUpperCase())) {
        xhr_settings['headers'] = {'X-CSRFToken' : getCookie('csrftoken')};
    }
    return $.ajax(xhr_settings).done(success).fail(fail);
}

/* Callbacks pour afficher à l'utilisateur un message renvoyé par l'API */
function showSuccess(jqxhr) {display_message(jqxhr.responseJSON.detail, 'success');}
function showInfo(jqxhr) {display_message(jqxhr.responseJSON.detail, 'info');}
function showWarning(jqxhr) {display_message(jqxhr.responseJSON.detail, 'warning');}
function showError(jqxhr) {display_message(jqxhr.responseJSON.detail, 'error');}

/* Callback pour ne rien faire des données renvoyées par l'API */
function doNothing(jqxhr) {}
