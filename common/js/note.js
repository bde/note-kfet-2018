/* Fonction pour formatter les chaînes de caractère */
String.prototype.format = String.prototype.format ||
function () {
    "use strict";
    var str = this.toString();
    if (arguments.length) {
        var t = typeof arguments[0];
        var key;
        var args = ("string" === t || "number" === t) ?
            Array.prototype.slice.call(arguments)
            : arguments[0];

        for (key in args) {
            str = str.replace(new RegExp("\\{" + key + "\\}", "gi"), args[key]);
        }
    }

    return str;
};

/* Fonction permettant de capitaliser une chaîne de caractères */
String.prototype.capitalize = String.prototype.capitalize ||
function () {
    "use strict";
    return this.charAt(0).toUpperCase() + this.slice(1);
};

/* Fonction permettant de retirer toutes les occurences d'un élément dans un tableau */
Array.prototype.remove = Array.prototype.remove ||
function (element) {
    "use strict";
    return this.filter(item => item !== element);
}

/* Fonction permettant de marquer une input comme valide */
function validInput(dom_id) {
    "use strict";
    $(dom_id).removeClass('is-invalid').addClass('is-valid');
}

/* Fonction permettant de marquer une input comme invalide */
function invalidInput(dom_id) {
    "use strict";
    $(dom_id).removeClass('is-valid').addClass('is-invalid');
}

/* Fonction permettant de retirer le marquage d'une input */
function clearInputState(dom_id) {
    "use strict";
    $(dom_id).removeClass('is-invalid').removeClass('is-valid');
}

/* Fonction permettant d'afficher des messages en haut de page */
function display_message(message, level) {
    "use strict";
    var css_level = {
        debug : 'debug', info : 'info', success : 'success', warning : 'warning', error : 'danger',
    };
    var _alert = $(document.createElement("div")).text(message);
    _alert.addClass("alert alert-{0} alert-dismissible fade show".format(css_level[level]));
    _alert.attr('role', 'alert');
    var cross = $(document.createElement("button")).append($(document.createElement("span")).html("&times;"));
    cross.addClass('close');
    cross.attr('data-dismiss', 'alert').attr('aria-hidden', 'true').attr('aria-label', 'Close');
    _alert.prepend(cross);
    $('#idMessageContainer').prepend(_alert);
}

/* Fonction pour récupérer la valeur d'un cookie
 * Récupérée depuis https://docs.djangoproject.com/fr/1.11/ref/csrf/ */
function getCookie(name) {
    "use strict";
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = $.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
