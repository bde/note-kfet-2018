"""
    Vues de l'application « Consos »
"""

import django
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404
from django.contrib import messages

from django.db.models import F, Q

from django.views.generic.base import RedirectView
from django.views.generic.edit import FormView, CreateView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.contenttypes.models import ContentType

from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.decorators import detail_route

from django_filters.rest_framework import DjangoFilterBackend

from note_kfet.utils import to_bool
from note_kfet.droits import D, Acl
from note_kfet.filters import NoteSearchFilter
from note_kfet.views.mixins import NoteMixin
from note_kfet.views.decorators import requires

from consos.models import Note, Transaction, Bouton
from consos.forms import BoutonConsoForm, CreditConsoForm, RetraitConsoForm, TransfertConsoForm
from consos.serializers import NoteSerializer, BoutonSerializer, TransactionSerializer

####################################################################################################
##                                  Vues de l'interface Web                                       ##
####################################################################################################

class NoteOuvrirView(PermissionRequiredMixin, LoginRequiredMixin, RedirectView):
    """
        Une vue permettant de d'ouvrir une note pour un adhérent existant dans
        la base de données
    """
    permanent = False
    pattern_name = "comptes:detail"

    permission_required = D("consos.note_ouvrir", Acl.BASIQUE)

    def get_redirect_url(self, *args, **kwargs):
        """
            Essaie de créer une note à un adhérent.
        """
        adh_class = ContentType.objects.get(model="adherent").model_class()
        if get_object_or_404(adh_class, id=kwargs['pk']).type == adh_class.DEBIT:
            messages.error(self.request, "Impossible d'ouvrir une note pour un compte de débit")
            return super().get_redirect_url(*args, **kwargs)
        note, created = Note.objects.get_or_create(adherent_id=kwargs['pk'])
        if not created:
            messages.warning(self.request, "L'adhérent dispose déjà d'une note")
        else:
            messages.success(self.request, "La note de l'adhérent a été ouverte.")
        return super().get_redirect_url(*args, **kwargs)

class NoteSoftLockView(PermissionRequiredMixin, LoginRequiredMixin, RedirectView):
    """
        Une vue permettant de poser un soft lock sur une note.
    """
    permanent = False
    pattern_name = "comptes:detail"

    permission_required = D("consos.note_soft_lock", Acl.LIMITE)

    @staticmethod
    def lock_note(note):
        if note.soft_lock:
            messages.warning(self.request, "Cette note était déjà protégée.")
        note.soft_lock = True
        note.save()

    def get_redirect_url(self, *args, **kwargs):
        note = get_object_or_404(Note, adherent_id=kwargs['pk'])
        user = self.request.user
        if (user.note == note) or user.has_perm("consos.note_soft_lock", Acl.TOTAL):
            self.lock_note(note)
        else:
            messages.error(self.request, "Vous ne pouvez pas protéger cette note")
        return super().get_redirect_url(*args, **kwargs)

class NoteHardLockView(PermissionRequiredMixin, LoginRequiredMixin, RedirectView):
    """
        Une vue permettant de poser un hard lock sur une note.
    """
    permanent = False
    pattern_name = "comptes:detail"

    permission_required = D("consos.note_hard_lock", Acl.BASIQUE)

    @staticmethod
    def lock_note(note):
        if note.hard_lock:
            messages.warning(self.request, "Cette note était déjà bloquée.")
        note.hard_lock = True
        note.save()

    def get_redirect_url(self, *args, **kwargs):
        note = get_object_or_404(Note, adherent_id=kwargs['pk'])
        user = self.request.user
        if (note.adherent.type == "personne") or user.has_perm("consos.note_hard_lock", Acl.TOTAL):
            self.lock_note(note)
        else:
            messages.error(self.request, "Vous ne pouvez pas bloquer cette note")
        return super().get_redirect_url(*args, **kwargs)

class BoutonConsoView(NoteMixin, PermissionRequiredMixin, LoginRequiredMixin, FormView):
    """
        Une vue permettant de noter des consos avec des boutons
    """
    app_label = "consos"

    form_class = BoutonConsoForm
    success_url = reverse_lazy("consos:consos")

    permission_required = D("consos.transaction_effectuer", Acl.BASIQUE)

    template_name = "consos.html"

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        ctxt = super().get_context_data(**kwargs)
        ctxt.update({'transactions' : Transaction.objects.order_by('-date')})
        return ctxt

class CreditConsoView(NoteMixin, PermissionRequiredMixin, LoginRequiredMixin, FormView):
    """
        Une vue permettant de créditer des notes d'adhérents.
    """
    app_label = "consos"

    form_class = CreditConsoForm
    success_url = reverse_lazy("consos:créditer")

    permission_required = D("consos.transaction_effectuer", Acl.BASIQUE)

    template_name = "consos.html"

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        ctxt = super().get_context_data(**kwargs)
        ctxt.update({'transactions' : Transaction.objects.order_by('-date')})
        return ctxt

class RetraitConsoView(NoteMixin, PermissionRequiredMixin, LoginRequiredMixin, FormView):
    """
        Une vue permettant de retirer de l'argent sur les notes des adhérents.
    """
    app_label = "consos"

    form_class = RetraitConsoForm
    success_url = reverse_lazy("consos:retirer")

    permission_required = D("consos.transaction_effectuer", Acl.BASIQUE)

    template_name = "consos.html"

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        ctxt = super().get_context_data(**kwargs)
        ctxt.update({'transactions' : Transaction.objects.order_by('-date')})
        return ctxt

class TransfertConsoView(NoteMixin, PermissionRequiredMixin, LoginRequiredMixin, FormView):
    """
        Une vie permettant de transférer de l'argent entre plusieurs notes.
    """
    app_label = "consos"

    form_class = TransfertConsoForm
    success_url = reverse_lazy("consos:transférer")

    permission_required = D("consos.transaction_effectuer", Acl.BASIQUE)

    template_name = "consos.html"

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        ctxt = super().get_context_data(**kwargs)
        ctxt.update({'transactions' : Transaction.objects.order_by('-date')})
        return ctxt

class DevalideTransactionView(PermissionRequiredMixin, LoginRequiredMixin, RedirectView):
    """
        Dévalide une transaction
    """
    permanent = False
    pattern_name = "consos:consos"

    permission_required = D("consos.transaction_devalider", Acl.BASIQUE)

    def get_redirect_url(self, *args, **kwargs):
        transaction = get_object_or_404(Transaction, pk=kwargs.pop('pk'))
        if transaction.valide:
            transaction.valide = False
            if transaction.emetteur:
                transaction.emetteur.solde = F('solde') + transaction.quantite*transaction.montant
                transaction.emetteur.save()
            if transaction.destinataire:
                transaction.destinataire.solde = F('solde') - transaction.quantite*transaction.montant
                transaction.destinataire.save()
            transaction.save()
            messages.success(self.request, "Transaction dévalidée avec succès")
        else:
            messages.error(self.request, "Transaction déjà dévalidée")
        return super().get_redirect_url(*args, **kwargs)

class ValideTransactionView(PermissionRequiredMixin, LoginRequiredMixin, RedirectView):
    """
        Valide une transaction
    """
    permanent = False
    pattern_name = "consos:consos"

    permission_required = D("consos.transaction_devalider", Acl.BASIQUE)

    def get_redirect_url(self, *args, **kwargs):
        transaction = get_object_or_404(Transaction, pk=kwargs.pop('pk'))
        if not transaction.valide:
            transaction.valide = True
            if transaction.emetteur:
                transaction.emetteur.solde = F('solde') - transaction.quantite*transaction.montant
                transaction.emetteur.save()
            if transaction.destinataire:
                transaction.destinataire.solde = F('solde') + transaction.quantite*transaction.montant
                transaction.destinataire.save()
            transaction.save()
            messages.success(self.request, "Transaction validée avec succès")
        else:
            messages.error(self.request, "Transaction déjà validée")
        return super().get_redirect_url(*args, **kwargs)

class BoutonGestionView(NoteMixin, LoginRequiredMixin, CreateView):
    """
        Une vue pour la gestion des boutons.
    """
    app_label = "consos"

    model = Bouton
    fields = ['etiquette', 'description', 'categorie', 'montant', 'credite', 'actif']
    success_url = reverse_lazy('consos:bouton_gestion')

    template_name = "boutons.html"

    def form_valid(self, form):
        user = self.request.user
        if user.has_perm("consos.bouton_ajouter", Acl.BASIQUE):
            return super().form_valid(form)
        else:
            messages.error(self.request, "Vous n'avez pas le droit de créer ce bouton")
            return self.form_invalid(form)

    def get_context_data(self, **kwargs):
        ctxt = super().get_context_data(**kwargs)
        ctxt.update({'boutons' : Bouton.objects.order_by('categorie', 'etiquette')})
        return ctxt

class BoutonModifierView(NoteMixin, PermissionRequiredMixin, LoginRequiredMixin, UpdateView):
    """
        Une vue pour modifier un bouton
    """
    app_label = "consos"

    model = Bouton
    fields = ['etiquette', 'description', 'categorie', 'montant', 'credite', 'actif']
    success_url = reverse_lazy('consos:bouton_gestion')

    permission_required = D("consos.bouton_modifier", Acl.BASIQUE)

    template_name = "boutons.html"

    def get_context_data(self, **kwargs):
        ctxt = super().get_context_data(**kwargs)
        ctxt.update({'boutons' : Bouton.objects.order_by('categorie', 'etiquette')})
        return ctxt

class BoutonSupprimerView(PermissionRequiredMixin, LoginRequiredMixin, RedirectView):
    """
        Une vue pour supprimer un bouton
    """
    permanent = False
    pattern_name = "consos:bouton_gestion"

    permission_required = D("consos.bouton_supprimer", Acl.BASIQUE)

    def get_redirect_url(self, *args, **kwargs):
        bouton = get_object_or_404(Bouton, pk=kwargs.pop('pk'))
        bouton.delete()
        return super().get_redirect_url(*args, **kwargs)

####################################################################################################
##                                          Vues de l'API                                         ##
####################################################################################################

class NoteViewSet(viewsets.GenericViewSet):
    """
        Ensemble de vues concernant la gestion des notes
    """
    queryset = Note.objects.all()
    serializer_class = NoteSerializer
    filter_backends = (NoteSearchFilter,)
    search_fields = ('alias',)

    @requires("comptes.adherent_chercher", Acl.BASIQUE)
    def list(self, request):
        """
            Renvoie la liste de toutes les notes actives
            Les données doivent :
                -   être envoyées avec une requête GET
                -   contenir un paramètre 'regex' pouvant valoir 'true' ou 'false'
                    indiquant si on doit effectuer une recherche par expression régulière ou non
                -   contenir un paramètre 'search' contenant le terme à rechercher
        """
        lookup_args = {}
        if not request.user.has_perm("comptes.adherent_chercher", Acl.TOTAL):
            lookup_args.update(**{'adherent__supprime' : False})

        if not request.user.has_perm("comptes.adherent_chercher", Acl.ETENDU):
            lookup_args.update(**{'adherent__is_active' : True})

        note_qs = self.get_queryset().filter(**lookup_args)
        alias_class = ContentType.objects.get(model="alias").model_class()
        qs = self.filter_queryset(
            alias_class.objects
            .filter(proprietaire__note__in=note_qs)
            .order_by('alias')
        )
        qs = qs.select_related('proprietaire__note')

        try:
            data = [
                {
                    'alias' : alias.alias,
                    'pseudo' : alias.proprietaire.pseudo,
                    'solde' : alias.proprietaire.note.solde,
                    'adherent' : alias.proprietaire.note.adherent,
                } for alias in qs
            ]
        except django.db.DataError as exc:
            return Response(
                {
                    "detail" : "%s" % exc.args,
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
        fields = ['adherent', 'pseudo', 'alias', 'solde']
        serializer = self.get_serializer(data, fields=fields, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @requires("comptes.adherent_detail")
    def retrieve(self, request, pk=None):
        """
            Renvoie les informations sur la note demandée
            Les données doivent :
                -   être envoyées via une requête GET
        """
        note = self.get_object()
        if (request.user != note.adherent
                and not request.user.has_perm("comptes.adherent_detail", Acl.BASIQUE)):
            return Response(
                {
                    "detail" : "Vous ne pouvez visualiser que votre propre note",
                },
                status=status.HTTP_403_FORBIDDEN,
            )

        fields = ['solde', 'releve_freq', 'dernier_releve', 'soft_lock', 'hard_lock',]
        serializer = self.get_serializer(note, fields=fields)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @requires("consos.note_ouvrir", Acl.BASIQUE)
    def create(self, request):
        """
            Ouvre la note d'un adhérent donné
            Les données doivent :
                -   être envoyées via une requête POST
                -   contenir un paramètre 'adherent' contenant l'identifiant de l'adhérent
                    dont il faut ouvrir la note
        """
        serializer = self.get_serializer(data=request.data, fields=['adherent'])
        serializer.is_valid(raise_exception=True)
        instance = serializer.save()
        return Response({"id" : instance.id}, status=status.HTTP_201_CREATED)

    @detail_route(methods=['patch'])
    @requires("consos.note_soft_lock")
    def protect(self, request, pk=None):
        """
            Protège une note, i.e. empêche qu'une transaction ne puisse être effectuée dessus
            Les données doivent :
                -   être envoyées via une requête PATCH
                -   contenir un paramètre 'soft_lock' pouvant valoir 'true' ou 'false'
        """
        note = self.get_object()
        if (note.adherent != request.user
                and not request.user.has_perm("consos.note_soft_lock", Acl.TOTAL)):
            return Response(
                {
                    "detail" : "Vous ne pouvez protéger que vôtre propre note",
                },
                status=status.HTTP_403_FORBIDDEN,
            )

        if 'soft_lock' not in request.data:
            return Response(
                {
                    "detail" : "Paramètre 'soft_lock' manquant",
                },
                status=status.HTTP_400_BAD_REQUEST,
            )

        fields = ['soft_lock']
        serializer = self.get_serializer(note, data=request.data, partial=True, fields=fields)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({}, status=status.HTTP_200_OK)

    @detail_route(methods=['patch'])
    @requires("consos.note_hard_lock", Acl.BASIQUE)
    def block(self, request, pk=None):
        """
            Bloque une note, i.e. empêche que la note ne puisse être utilisée.
            Le blocage est posé par un membre de l'association et ne peut être retiré
            par l'adhérent.
            Les données doivent :
                -   être envoyées via une requête PATCH
                -   contenir un paramètre 'hard_lock' pouvant valoir 'true' ou 'false'
        """
        note = self.get_object()
        if (note.adherent.type != note.adherent.PERSONNE
                and not request.user.has_perm("consos.note_hard_lock", Acl.TOTAL)):
            return Response(
                {
                    "detail" : "Vous ne pouvez bloquer que les adhérents de type personne",
                },
                status=status.HTTP_403_FORBIDDEN,
            )

        if 'hard_lock' not in request.data:
            return Response(
                {
                    "detail" : "Paramètre 'hard_lock' manquant",
                },
                status=status.HTTP_400_BAD_REQUEST,
            )

        fields = ['hard_lock']
        serializer = self.get_serializer(note, data=request.data, partial=True, fields=fields)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({}, status=status.HTTP_200_OK)

class BoutonViewSet(viewsets.GenericViewSet):
    """
        Ensemble de vues pour la gestion des boutons
    """
    queryset = Bouton.objects.all()
    serializer_class = BoutonSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = {
        'categorie' : ['exact', 'in'],
        'actif' : ['exact'],
    }

    def list(self, request):
        """
            Renvoie la liste des boutons disponibles dans la base de données
            Les données doivent :
                -   être envoyées via une requête GET
        """
        if not request.user.has_perm(
                D("consos.bouton_ajouter", Acl.LIMITE)
                | D("consos.bouton_modifier", Acl.LIMITE)
                | D("consos.bouton_supprimer", Acl.LIMITE)
                | D("consos.bouton_desactiver", Acl.LIMITE)
                | D("consos.transaction_effectuer", Acl.TOTAL)):
            return Response({}, status=status.HTTP_403_FORBIDDEN)

        if not request.user.has_perm(
                D("consos.bouton_ajouter", Acl.BASIQUE)
                | D("consos.bouton_modifier", Acl.BASIQUE)
                | D("consos.bouton_supprimer", Acl.BASIQUE)
                | D("consos.bouton_desactiver", Acl.BASIQUE)
                | D("consos.transaction_effectuer", Acl.TOTAL)):
            # L'utilisateur ne peut que consulter les boutons liés à sa note
            qs = self.get_queryset().filter(credite=request.user)
        else:
            qs = self.get_queryset()
        qs = self.filter_queryset(qs).order_by('categorie', 'etiquette')
        serializer = self.get_serializer(qs, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @requires("consos.bouton_ajouter")
    def create(self, request):
        """
            Ajoute un nouveau bouton dans la base de données
            Les données doivent :
                -   être envoyées via une requête POST
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        note_id = serializer.validated_data['credite']
        if note_id is None and not request.user.has_perm("consos.bouton_ajouter", Acl.BASIQUE):
            # Le bouton devrait pointer vers l'association, mais
            # l'utilisateur n'a pas le droit qu'il faut
            return Response(
                {
                    "detail" : "Vous n'avez pas le droit de lier un bouton à "
                               "une autre note que la vôtre",
                },
                status=status.HTTP_403_FORBIDDEN,
            )
        elif note_id is None:
            # Le bouton pointera vers l'association
            pass
        else:
            # Le bouton pointe vers une autre note
            note = Note.objects.get(pk=note_id)
            if note.proprietaire != request.user and not request.user.has_perm("consos.bouton_ajouter", Acl.BASIQUE):
                return Response({"detail" : "Vous n'avez pas le droit de lier un bouton à une autre note que la vôtre"}, status=status.HTTP_403_FORBIDDEN)
            if note.type not in [note.proprietaire.CLUB, note.proprietaire.SECTION] and not request.user.has_perm("consos.bouton_ajouter", Acl.TOTAL):
                return Response({"detail" : "Vous n'avez pas le droit de lier un bouton à autre chose qu'un club ou une section"}, status=status.HTTP_403_FORBIDDEN)
        instance = serializer.save()
        return Response({"id" : instance.id}, status=status.HTTP_201_CREATED)

    @requires("consos.bouton_modifer")
    def partial_update(self, request, pk=None):
        """
            Modifie un bouton existant
        """
        bouton = self.get_object()
        serializer = self.get_serializer(bouton, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        if bouton.credite.pk != serializer.validated_data['credite'] and not request.user.has_perm("consos.bouton_modifier", Acl.TOTAL):
            return Response({"detail" : "Vous n'avez pas le droit de changer la note liée à un bouton"}, status=status.HTTP_403_FORBIDDEN)
        if bouton.credite != request.user and not request.user.has_perm("consos.bouton_modifier", Acl.BASIQUE):
            return Response({"detail" : "Vous ne pouvez modifier que les boutons liés à vôtre note"}, status=status.HTTP_403_FORBIDDEN)
        serializer.save()

    @detail_route(methods=['patch'])
    @requires("consos.bouton_activer")
    def activate(self, request, pk=None):
        """
            Active ou désactive un bouton
        """
        activate = to_bool(request.query_params.get('activate', None))
        if activate is None:
            return Response({"detail" : "Paramètre 'activate' incorrect"}, status=status.HTTP_400_BAD_REQUEST)

        bouton = self.get_object()
        bouton.actif = activate
        bouton.save()
        return Response({}, status=status.HTTP_200_OK)

    @requires("consos.bouton_supprimer")
    def destroy(self, request, pk=None):
        """
            Supprime un bouton existant
        """
        bouton = self.get_object()
        if bouton.credite != request.user and not request.user.has_perm("consos.bouton_supprimer", Acl.BASIQUE):
            return Response({"detail" : "Vous ne pouvez supprimer que des boutons liés à vôtre note"}, status=status.HTTP_403_FORBIDDEN)
        bouton.delete()
        return Response({}, status=status.HTTP_204_NO_CONTENT)

class TransactionViewSet(viewsets.GenericViewSet):
    """
        Ensemble de vues pour la gestion des transactions
    """
    queryset = Transaction.objects.all()
    serializer_class = TransactionSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('id', 'valide',)

    @requires("consos.transaction_effectuer")
    def list(self, request):
        """
            Renvoie la liste des transactions existantes
        """
        qs = self.get_queryset()
        if not request.user.has_perm("consos.transaction_effectuer", Acl.TOTAL):
            qs = qs.filter(Q(emetteur=request.user) | Q(destinataire=request.user))
        qs = self.filter_queryset(qs).order_by('-date')
        serializer = self.get_serializer(qs, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @requires("consos.transaction_effectuer")
    def create(self, request):
        """
            Effectue une ou plusieurs nouvelles transactions.
            S'il s'agit d'une vente de consos, le paramètre 'action' doit valoir
            'vente', et le 'moyen' de paiement doit valoir 'note'.
            Les données doivent:
                -   être envoyées via une requête POST
                -   dans le cas d'une vente, contenir l'identifiant du bouton dans le paramètre 'bouton',
                    contenir une liste d'identifiants de note dans le paramètre 'emetteur', ainsi qu'une quantité
                    dans le paramètre 'quantite'
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        if (any(emetteur != request.user for emetteur in serializer.validated_data.get('emetteurs', []))
                and not request.user.has_perm("consos.transaction_effectuer", Acl.TOTAL)):
            return Response({"detail" : "Vous ne pouvez faire que des transactions depuis vôtre note"}, status=status.HTTP_403_FORBIDDEN)
        instances = serializer.save()
        return Response({"id" : [instance.id for instance in instances]}, status=status.HTTP_201_CREATED)

    @detail_route(methods=['patch'])
    @requires("consos.transaction_devalider")
    def valide(self, request, pk=None):
        """
            Valide/dévalide une transaction
        """
        transaction = self.get_object()
        fields = ['valide']
        serializer = self.get_serializer(transaction, data=request.data, partial=True, fields=fields)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({}, status=status.HTTP_200_OK)

class ConsosViewSet(viewsets.GenericViewSet):
    """
        Ensemble de vues permettant de voir les transactions liées à un adhérent
    """
    queryset = Transaction.objects.all()
    serializer_class = TransactionSerializer

    def get_queryset(self):
        """
            Filtre le QuerySet de façon à n'inclure que les transactions impliquant
            la note d'un adhérent donné.
        """
        note = self.kwargs['note_pk']
        return super().get_queryset().filter(Q(emetteur=note) | Q(destinataire=note))

    @requires("comptes.adherent_detail")
    def list(self, request, note_pk=None):
        """
            Renvoie la liste ordonnée des transacations effectuées sur la note d'un adhérent
        """
        note = Note.objects.get(pk=note_pk)
        if (note.adherent != request.user
                and not request.user.has_perm("comptes.adherent_detail", Acl.BASIQUE)):
            return Response(
                {
                    "detail" : "Vous ne pouvez consulter que les transactions "
                               "liées à vôtre propre note",
                },
                status=status.HTTP_403_FORBIDDEN,
            )
        qs = self.get_queryset().order_by('-date')
        serializer = self.get_serializer(qs, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
