"""
    Sérialiseurs de l'application « Consos »
"""

import itertools

from django.db.models import Q
from django.contrib.auth import get_user_model

from rest_framework import serializers

from note_kfet.serializers import mixins

from consos.models import Note, Bouton, Transaction

UserModel = get_user_model()

class NoteSerializer(mixins.DynamicFieldsMixin, serializers.ModelSerializer):
    """
        Sérialiseur pour les objets Note
    """
    alias = serializers.CharField(read_only=True)
    pseudo = serializers.CharField(source='adherent.pseudo', read_only=True)
    class Meta:
        model = Note
        fields = [
            'adherent', 'solde', 'releve_freq', 'dernier_releve',
            'soft_lock', 'hard_lock', 'alias', 'pseudo',
        ]
        read_only_fields = ['alias', 'pseudo', 'solde',]
        default_empty = False
        extra_kwargs = {
            'adherent' : {
                'queryset' : UserModel.objects.filter(~Q(type=UserModel.DEBIT) & Q(supprime=False))
            },
        }

class BoutonSerializer(serializers.ModelSerializer):
    """
        Sérialiseur pour les objets Bouton
    """
    class Meta:
        model = Bouton
        fields = [
            'id', 'etiquette', 'description', 'categorie',
            'montant', 'credite', 'actif',
        ]
        read_only_fields = ['id', 'actif']

class TransactionSerializer(mixins.DynamicFieldsMixin, serializers.ModelSerializer):
    """
        Sérialiseur pour les transactions
    """
    emetteurs = serializers.PrimaryKeyRelatedField(
        many=True, allow_null=True, write_only=True,
        queryset=Note.objects.filter(soft_lock=False, hard_lock=False),
    )
    destinataires = serializers.PrimaryKeyRelatedField(
        many=True, allow_null=True, write_only=True,
        queryset=Note.objects.filter(soft_lock=False, hard_lock=False),
    )
    bouton = serializers.PrimaryKeyRelatedField(
        write_only=True, queryset=Bouton.objects.filter(actif=True),
    )
    class Meta:
        model = Transaction
        fields = [
            'id', 'date', 'action', 'moyen', 'quantite',
            'montant', 'categorie', 'description', 'valide', 'emetteur',
            'destinataire', 'emetteurs', 'destinataires', 'bouton',
        ]
        read_only_fields = ['date', 'emetteur', 'destinataire']
        default_empty = False

    def create(self, validated_data):
        params = {
            'action' : validated_data.get('action'),
            'quantite' : validated_data.get('quantite'),
            'description' : '(%s)' if validated_data.get('description') else "",
            'valide' : validated_data.get('valide'),
        }
        emetteurs = []
        destinataires = []

        if params['action'] == Transaction.VENTE:
            bouton = validated_data.get('bouton')
            params['moyen'] = Transaction.NOTE
            params['montant'] = bouton.montant
            params['categorie'] = bouton.categorie
            params['description'] = bouton.etiquette
            emetteurs = validated_data.get('emetteurs')
            destinataires = [bouton.credite]
        elif params['action'] == Transaction.CREDIT:
            params['moyen'] = validated_data.get('moyen')
            params['montant'] = validated_data.get('montant')
            params['categorie'] = "Opération de gestion"
            params['description'] = "Crédit Note"
            emetteurs = [Note.ASSOCIATION]
            destinataires = validated_data.get('destinataires')
        elif params['action'] == Transaction.RETRAIT:
            params['moyen'] = validated_data.get('moyen')
            params['montant'] = validated_data.get('montant')
            params['categorie'] = "Opération de gestion"
            params['description'] = "Retrait Note"
            emetteurs = validated_data.get('emetteurs')
            destinataires = [Note.ASSOCIATION]
        elif params['action'] == Transaction.TRANSFERT:
            params['moyen'] = validated_data.get('moyen')
            params['montant'] = validated_data.get('montant')
            params['categorie'] = "Opération de gestion"
            params['description'] = "Transfert %s" % params['description']
            emetteurs = validated_data.get('emetteurs')
            destinataires = validated_data.get('destinataires')
        else:
            raise serializers.ValidationError(
                {'action' : "Action %r non prise en charge" % params['action']},
            )

        created = []
        for emetteur, destinataire in itertools.product(emetteurs, destinataires):
            transaction = Transaction(emetteur=emetteur, destinataire=destinataire, **params)
            transaction.full_clean(exclude=['id'])
            transaction.save()
            created.append(transaction)

        return created
