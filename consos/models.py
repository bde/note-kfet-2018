"""
    Modèles de l'application Consos
"""

from datetime import timedelta

from django.db import models, transaction
from django.db.models import Q
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError

def releve_freq_validator(value):
    """
        Vérifie que la durée donnée n'est pas plus petite que l'intervalle
        minimum supporté pour la note.
    """
    if value < timedelta(days=1):
        raise ValidationError(
            "L'intervalle entre deux relevés de consommation "
            "doit être supérieur à une journée."
        )

class Note(models.Model):
    """
        Une note associée avec un adhérent

        -   adherent        [int*]      :   L'adhérent associé à la note
        -   solde           [Decimal]   :   Solde de la note
        -   releve_freq     [interval]  :   Fréquence des relevés de consommation
        -   dernier_releve  [tstz]      :   Date du dernier relevé de consommation
        -   soft_lock       [boolean]   :   Verrou posé par le proprio pour empêcher les consos
        -   hard_lock       [boolean]   :   Verrou posé par le BdE pour empêcher les consos
    """
    # Constantes utiles pour l'abstraction
    ASSOCIATION = None

    # Champs du modèle
    adherent = models.OneToOneField(
        'comptes.Adherent', on_delete=models.CASCADE, verbose_name="propriétaire",
        primary_key=True, limit_choices_to=~Q(type=get_user_model().DEBIT) & Q(supprime=False),
    )
    solde = models.DecimalField(max_digits=14, decimal_places=2, null=False, default=0.00)
    releve_freq = models.DurationField(
        'fréquence des relevés de consommation', null=True, blank=True,
        default=None, validators=[releve_freq_validator],
    )
    dernier_releve = models.DateTimeField(
        'dernier relevé de consommation', null=True, blank=True, default=None
    )
    soft_lock = models.BooleanField('verrouillé par le propriétaire', default=False)
    hard_lock = models.BooleanField("verrouillé par l'association", default=False)

    class Meta:
        default_permissions = []
        permissions = [
            ('note_ouvrir', "Activer la note d'un adhérent"),
            ('note_soft_lock', "Protéger la note d'un adhérent"),
            ('note_hard_lock', "Bloquer la note d'un adhérent"),
        ]

    def get_solde_display(self):
        return "{} €".format(self.solde)

    def __str__(self):
        return str(self.adherent)

def quantite_minimale(value):
    """
        Vérifie que le quantité fournie est plus grande que 1
    """
    if value < 1:
        raise ValidationError("La quantité ne peut être inférieure à 1")

class Transaction(models.Model):
    """
        Une transaction stockée dans la base de données.
        ATTENTION : La transaction est effectuée au moment de l'appel à save()

        -   date        [tstz]          :   Date de la transaction
        -   action      [varchar(255)]  :   Action effectuée (Crédit, Débit, ...)
        -   moyen       [varchar(255)]  :   Moyen de paiement utilisé
        -   quantite    [int]           :   Quantité de produit échangée
        -   montant     [Decimal]       :   Prix unitaire du produit
        -   categorie   [varchar(255)]  :   Catégorie du produit
        -   description [varchar]       :   Commentaire/Description de la transaction
        -   valide      [boolean]       :   La transaction est-elle valide ?
        -   emetteur    [int*]          :   Émetteur de la transaction
        -   destinataire[int*]          :   Destinataire de la transaction
    """
    ## Moyens de paiement
    NOTE = "note"
    CHEQUE = "chèque"
    ESPECES = "espèces"
    VIREMENT = "virement"
    CB = "carte bancaire"

    MOYENS_DE_PAIEMENT = [
        (NOTE, NOTE),
        (CHEQUE, CHEQUE),
        (ESPECES, ESPECES),
        (VIREMENT, VIREMENT),
        (CB, CB),
    ]

    ## Actions
    VENTE = "vente"
    CREDIT = "crédit"
    RETRAIT = "retrait"
    TRANSFERT = "transfert"

    ACTIONS = [
        (VENTE, VENTE),
        (CREDIT, CREDIT),
        (RETRAIT, RETRAIT),
        (TRANSFERT, TRANSFERT),
    ]

    ## Champs du modèle
    date = models.DateTimeField(auto_now_add=True, null=False)
    action = models.CharField(max_length=255, null=False, blank=False, choices=ACTIONS)
    moyen = models.CharField(max_length=255, null=False, blank=False, choices=MOYENS_DE_PAIEMENT)
    quantite = models.IntegerField(
        "quantité", null=False, blank=False, validators=[quantite_minimale] # XXX: Valeur par défaut = 1 ?
    )
    montant = models.DecimalField(max_digits=11, decimal_places=2, null=False, blank=False)
    categorie = models.CharField(max_length=255, null=False, blank=False)
    description = models.TextField(null=False, blank=True)
    valide = models.BooleanField(default=True)
    emetteur = models.ForeignKey(
        'consos.Note', on_delete=models.CASCADE,
        null=True, blank=True, related_name="transactions_emises",
    )
    destinataire = models.ForeignKey(
        'consos.Note', on_delete=models.CASCADE,
        null=True, blank=True, related_name="transactions_recues",
    )

    # Variables internes
    _was_valid = False

    class Meta:
        default_permissions = []
        permissions = [
            ('transaction_effectuer', 'Peut effectuer des transactions'),
            ('transaction_devalider', 'Peut valider/dévalider des transactions'),
        ]

    def __str__(self):
        return "Transaction #%d le %s : %d %s de %s vers %s" % (
            self.id, self.date, self.quantite, self.description, self.emetteur, self.destinataire,
        )

    def clean(self):
        """
            Vérifie quelques éléments d'une transaction :
            -   L'émetteur et le destinataire doivent être distincts
        """
        if self.emetteur == self.destinataire:
            raise ValidationError(
                "L'émetteur et le destinataire d'une transaction doivent être distincts"
            )

    @classmethod
    def from_db(cls, db, field_names, values):
        instance = super(Transaction, cls).from_db(db, field_names, values)
        instance._was_valid = instance.valide
        return instance

    @transaction.atomic
    def _do_transaction(self):
        if self.emetteur is not None:
            self.emetteur.solde = models.F('solde') - self.quantite*self.montant
            self.emetteur.save(update_fields=['solde'])
        if self.destinataire is not None:
            self.destinataire.solde = models.F('solde') + self.quantite*self.montant
            self.destinataire.save(update_fields=['solde'])

    @transaction.atomic
    def _undo_transaction(self):
        if self.destinataire is not None:
            self.destinataire.solde = models.F('solde') - self.quantite*self.montant
            self.destinataire.save(update_fields=['solde'])
        if self.emetteur is not None:
            self.emetteur.solde = models.F('solde') + self.quantite*self.montant
            self.emetteur.save(update_fields=['solde'])

    def save(self, *args, **kwargs):
        """
            Enregistre la transaction et l'effectue si celle-ci est nouvelle
        """
        if self.valide and not self._was_valid:
            self._do_transaction()
        elif not self.valide and self._was_valid:
            self._undo_transaction()

        self._was_valid = self.valide
        super().save(*args, **kwargs)

class Bouton(models.Model):
    """
        Un bouton permettant de noter des consommations.

        -   etiquette   [varchar(255)]  :   Libellé de la conso.
        -   description [varchar]       :   Description de la fonction du bouton et/ou de la conso.
        -   categorie   [int*]          :   Catégorie à laquelle appartient la conso.
        -   montant     [Decimal]       :   Montant de la conso.
        -   credite     [int*]          :   Note créditée par ce bouton (~vendeur)
        -   actif       [boolean]       :   Ce bouton est-il actif ?
    """
    etiquette = models.CharField(max_length=255, null=False, blank=False)
    description = models.TextField(null=False, blank=True)
    categorie = models.CharField(max_length=255, null=False, blank=False)
    montant = models.DecimalField(max_digits=11, decimal_places=2, null=False, blank=False)
    credite = models.ForeignKey('consos.Note', on_delete=models.CASCADE, null=True, blank=True)
    actif = models.BooleanField(default=True)

    class Meta:
        unique_together = ('etiquette', 'categorie',)
        default_permissions = []
        permissions = [
            ('bouton_ajouter', "Peut ajouter un bouton"),
            ('bouton_modifier', "Peut modifier un bouton"),
            ('bouton_supprimer', 'Peut supprimer un bouton'),
            ('bouton_activer', 'Peut activer/désactiver un bouton'),
        ]

    def __str__(self):
        return "%s (%s)" % (self.etiquette, self.montant)

    def save(self, *args, **kwargs):
        """
            Enragistre un nouveau bouton, et la nouvelle catégorie à laquelle il appartient
            le cas échéant.
        """
        return super().save(*args, **kwargs)

class Cotisation(models.Model):
    """
        Une cotisation d'adhésion à l'association

        -   adhesion    [int*]  :   Adhésion référencée par cette cotisation
        -   transaction [int*]  :   Transaction associée à cette cotisation
    """
    adhesion = models.ForeignKey('comptes.Adhesion', on_delete=models.CASCADE, null=False)
    transaction = models.ForeignKey('consos.Transaction', on_delete=models.CASCADE, null=False)

    class Meta:
        default_permissions = []

    def __str__(self):
        return "Cotisation #%d (%s)" % (self.id, self.adhesion.adherent)
