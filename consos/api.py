"""
    Vues de l'API de l'application « Comptes »
"""

from django.conf.urls import url, include

from rest_framework_nested import routers

from consos.views import NoteViewSet, BoutonViewSet, TransactionViewSet, ConsosViewSet

router = routers.DefaultRouter()

router.register(r'notes', NoteViewSet)
router.register(r'boutons', BoutonViewSet)
router.register(r'transactions', TransactionViewSet)

note_router = routers.NestedSimpleRouter(router, r'notes', lookup="note")
note_router.register(r'transactions', ConsosViewSet, base_name="note-transactions")

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^', include(note_router.urls)),
]
