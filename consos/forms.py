"""
    Formulaires de l'application « Consos »
"""

import itertools

from django import forms
from django.db.models import F

from consos.models import Bouton, Note, Transaction

MOYENS_DE_PAIEMENT_CONSOS = [
    (Transaction.NOTE, Transaction.NOTE),
    (Transaction.ESPECES, Transaction.ESPECES),
    (Transaction.VIREMENT, Transaction.VIREMENT),
    (Transaction.CB, Transaction.CB),
]

class BoutonConsoForm(forms.Form):
    """
        Un formulaire pour noter de nouvelles consos
    """
    emetteurs = forms.ModelMultipleChoiceField(queryset=Note.objects.order_by('adherent_id').exclude(adherent__supprime=True), required=True)
    boutons = forms.ModelMultipleChoiceField(queryset=Bouton.objects.order_by('id'), required=True)
    quantite = forms.IntegerField(initial=1, required=True)

    def save(self, commit=True):
        for emetteur, bouton in itertools.product(self.cleaned_data['emetteurs'], self.cleaned_data['boutons']):
            transaction = Transaction.objects.create(
                emetteur=emetteur,
                destinataire=bouton.credite,
                action=Transaction.VENTE,
                moyen=Transaction.NOTE,
                quantite=self.cleaned_data['quantite'],
                montant=bouton.montant,
                description=bouton.etiquette,
                categorie=bouton.categorie,
                valide=True,
            )

class CreditConsoForm(forms.Form):
    """
        Un formulaire pour créditer des adhérents
    """
    destinataires = forms.ModelMultipleChoiceField(queryset=Note.objects.order_by('adherent_id').exclude(adherent__supprime=True))
    montant = forms.DecimalField(max_digits=14, decimal_places=2, required=True)
    moyen = forms.ChoiceField(choices=MOYENS_DE_PAIEMENT_CONSOS)

    def save(self, commit=True):
        for destinataire in self.cleaned_data['destinataires']:
            transaction = Transaction.objects.create(
                emetteur=None,
                destinataire=destinataire,
                action=Transaction.CREDIT,
                moyen=self.cleaned_data['moyen'],
                quantite=1,
                montant=self.cleaned_data['montant'],
                description="Crédit %s" % self.cleaned_data['moyen'],
                categorie="Opération de gestion",
                valide=True,
            )

class RetraitConsoForm(forms.Form):
    """
        Un formulaire pour retirer de l'argent à un adhérent
    """
    emetteurs = forms.ModelMultipleChoiceField(queryset=Note.objects.order_by('adherent_id').exclude(adherent__supprime=True))
    montant = forms.DecimalField(max_digits=14, decimal_places=2, required=True)
    moyen = forms.ChoiceField(choices=MOYENS_DE_PAIEMENT_CONSOS)

    def save(self, commit=True):
        for emetteur in self.cleaned_data['emetteurs']:
            transaction = Transaction.objects.create(
                emetteur=emetteur,
                destinataire=None,
                action=Transaction.RETRAIT,
                moyen=self.cleaned_data['moyen'],
                quantite=1,
                montant=self.cleaned_data['montant'],
                description="Retrait %s" % self.cleaned_data['moyen'],
                categorie="Opération de gestion",
                valide=True,
            )

class TransfertConsoForm(forms.Form):
    """
        Un formulaire pour effectuer des transferts entre les adhérents
    """
    emetteurs = forms.ModelMultipleChoiceField(queryset=Note.objects.order_by('adherent_id').exclude(adherent__supprime=True), required=True)
    destinataires = forms.ModelMultipleChoiceField(queryset=Note.objects.order_by('adherent_id').exclude(adherent__supprime=True), required=True)
    montant = forms.DecimalField(max_digits=14, decimal_places=2, required=True)
    description = forms.CharField(max_length=255)

    def save(self, commit=True):
        for emetteur, destinataire in itertools.product(self.cleaned_data['emetteurs'], self.cleaned_data['destinataires']):
            transaction = Transaction.objects.create(
                emetteur=emetteur,
                destinataire=destinataire,
                action=Transaction.TRANSFERT,
                moyen=Transaction.NOTE,
                quantite=1,
                montant=self.cleaned_data['montant'],
                description="Transfert : %s" % self.cleaned_data['description'],
                categorie="Opération de gestion",
                valide=True,
            )
