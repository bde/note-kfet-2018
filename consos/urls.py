"""
    Configuration de URLs de l'app « Consos »
"""

from django.conf.urls import url
from django.views.generic.base import RedirectView

from consos.views import (
    NoteOuvrirView, NoteSoftLockView, NoteHardLockView,
    BoutonConsoView, CreditConsoView, RetraitConsoView,
    TransfertConsoView, DevalideTransactionView, ValideTransactionView,
    BoutonGestionView, BoutonModifierView, BoutonSupprimerView,
)

urlpatterns = [
    url(r'^$', RedirectView.as_view(pattern_name="consos:consos"), name="accueil"),
    # Gestion des notes
    url(r'^(?P<pk>\d+)/ouvrir/$', NoteOuvrirView.as_view(), name="note_ouvrir"),
    url(r'^(?P<pk>\d+)/proteger/$', NoteSoftLockView.as_view(), name="note_proteger"),
    url(r'^(?P<pk>\d+)/bloquer/$', NoteHardLockView.as_view(), name="note_bloquer"),
    # Transactions
    url(r'^consos/$', BoutonConsoView.as_view(), name="consos"),
    url(r'^credits/$', CreditConsoView.as_view(), name="créditer"),
    url(r'^retraits/$', RetraitConsoView.as_view(), name="retirer"),
    url(r'^transferts/$', TransfertConsoView.as_view(), name="transférer"),
    url(r'^valider/(?P<pk>\d+)/$', ValideTransactionView.as_view(), name="valider"),
    url(r'^devalider/(?P<pk>\d+)/$', DevalideTransactionView.as_view(), name="dévalider"),
    # Boutons
    url(r'^boutons/$', BoutonGestionView.as_view(), name="bouton_gestion"),
    url(r'^boutons/(?P<pk>\d+)/$', BoutonModifierView.as_view(), name="bouton_modifier"),
    url(r'^boutons/(?P<pk>\d+)/supprimer/$', BoutonSupprimerView.as_view(), name="bouton_supprimer"),
]
