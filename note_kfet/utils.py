"""
    Fonctions et classes utilitaires
"""

from itertools import chain
from collections import OrderedDict

from distutils.util import strtobool

class CumulativeDict(OrderedDict):
    """
        Dictionnaires permettant de renvoyer toutes les valeurs
        associées à aux clefs plus petites que celle demandée.
    """
    def __getitem__(self, key):
        if key not in self:
            raise KeyError(key)
        values = []
        for dict_key in self:
            if dict_key <= key:
                values.append(super().__getitem__(dict_key))
        return list(chain.from_iterable(values))

    def __setitem__(self, key, value):
        if isinstance(value, list):
            super().__setitem__(key, value)
        elif isinstance(value, (tuple, set)):
            super().__setitem__(key, list(value))
        else:
            super().__setitem__(key, [value])

    def get(self, key, default=None):
        try:
            return self[key]
        except KeyError:
            return []

    def setdefault(self, key, default=None):
        if key not in self:
            self[key] = default or []
        return self[key]

    def append(self, key, value):
        if key not in self:
            raise KeyError
        self[key].append(value)

def to_bool(value):
    """
        Convertit une chaine de caractères représentant un booléen en un booléen Python
    """
    try:
        return bool(strtobool(value))
    except ValueError:
        return None

def escape_regex(regex):
    """
        Échappe les caractères spéciaux utilisés dans les expressions
        régulières POSIX (PREs)
    """
    escaped = ""
    for char in regex:
        if char in ['{', '}', '(', ')', '[', ']', '|', '?', '*', '.', '\\',]:
            escaped += '\\' + char
        else:
            escaped += char

    return escaped
