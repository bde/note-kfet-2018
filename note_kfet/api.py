"""
    Configuration des URLs de l'API de la Note Kfet
"""

from django.conf import settings
from django.conf.urls import url, include

from rest_framework import views, status
from rest_framework.reverse import reverse_lazy
from rest_framework.response import Response

# Dictionnaire contenant les apps disposant d'une API
APPS_API = ['comptes', 'activites', 'consos']

urlpatterns = [
    url('^%s/' % app, include(('%s.api' % app, '%s_api' % app), namespace=app)) for app in APPS_API
]

class NKAPIRootView(views.APIView):
    """
        Vue racine de l'API.
        Permet d'exposer toutes les applications disposant d'une API.
    """
    def get(self, request, format=None):
        """
            Renvoie une dictionnaire contenant la liste des applications ayant une API
            et le lien vers celles-ci
        """
        return Response(
            {
                app : "%s%s/" % (reverse_lazy('api:root', request=request), app) for app in APPS_API
            },
            status=status.HTTP_200_OK,
        )

urlpatterns += [
    url('^$', NKAPIRootView.as_view(), name="root"),
]
