#!/usr/bin/env python3
"""
    Peuplement initial de la base de données et mise en place des dossiers necéssaires
    au fonctionnement de la note
"""

import sys
sys.path.append("/var/www/note-kfet/")

import os

import django
from django.conf import settings as django_settings

import note_kfet.settings as note_settings

django_settings.configure(**{key : value for key, value in note_settings.__dict__.items() if key.isupper()})
django.setup()

from django.contrib.contenttypes.models import ContentType

from note_kfet import environnement

## Création des dossiers nécéssaires au fonctionnement de la note

DIRECTORIES = (
    # (Dossier, Mode)
    (django_settings.MEDIA_ROOT, 0o775),
    ("%savatars" % django_settings.MEDIA_ROOT, 0o775),
)

for directory, mode in DIRECTORIES:
    if not os.path.exists(directory):
        os.mkdir(directory, mode=mode)

## Sections

Section = ContentType.objects.get(app_label="comptes", model="section").model_class()
for sigle, nom, ferme in environnement.SECTIONS:
    Section.objects.get_or_create(sigle=sigle, nom=nom, ferme=ferme)

## Accréditations

Droit = ContentType.objects.get(app_label="comptes", model="droit").model_class()
for model, codename, niveau, description in environnement.ACCREDITATIONS:
    app_label, model = model.split('.')[-2:]
    content_type = ContentType.objects.get(app_label=app_label, model=model.lower())
    Droit.objects.get_or_create(**{
        'description' : description,
        'content_type' : content_type,
        'codename' : codename,
        'niveau' : niveau,
    })

### Actions
#
#Action = ContentType.objects.get(app_label="consos", model="action").model_class()
#for action, description in environnement.ACTIONS:
#    Action.objects.get_or_create(action=action, description=description)
#
### Moyens de paiement
#
#Paiement = ContentType.objects.get(app_label="consos", model="paiement").model_class()
#for moyen, description in environnement.PAIEMENTS:
#    Paiement.objects.get_or_create(moyen=moyen, description=description)
#
### Catégories
#
#Categorie = ContentType.objects.get(app_label="consos", model="categorie").model_class()
#for nom, description in environnement.CATEGORIES:
#    Categorie.objects.get_or_create(nom=nom, description=description)
