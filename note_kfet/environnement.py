"""
    Configuration de l'environnement dans lequel évolue cette
    instance de la Note Kfet.
"""

from note_kfet.droits import Acl

################################################################################
#                                   Sections                                   #
################################################################################

# (< Sigle >, < Nom >, < Section fermée >)

SECTIONS = [
    ('A0', 'Informatique', False),
    ('A1', 'Mathématiques', False),
    ('A2', 'Physique', False),
    ('A\'2', 'Physique Appliquée', True),
    ('A"2', 'Chimie', False),
    ('A3', 'Biologie', False),
    ('B1234', 'SAPHIRE', False),
    ('B1', 'Génie Mécanique', False),
    ('B2', 'Génie Civil', False),
    ('B3', 'Mécanique', False),
    ('B4', 'EEA', False),
    ('C', 'Design', False),
    ('D2', 'Économie-Gestion', False),
    ('D3', 'Sciences Sociales', False),
    ('E', 'Anglais', False),
    ('EXT', 'Autre', False),
]

################################################################################
#                               Accréditations                                 #
################################################################################

# (< Modèle >, < Nom de code >, <Niveau de confidentialité>, < Description >)

ACCREDITATIONS = [
## Accréditations sur les adhérents
    ('comptes.Adherent', 'adherent_inscrire', Acl.BASIQUE, "Peut inscrire des personnes"),
    ('comptes.Adherent', 'adherent_inscrire', Acl.ETENDU, "Peut inscrire des personnes, clubs et sections"),
    ('comptes.Adherent', 'adherent_inscrire', Acl.TOTAL, "Peut inscrire n'importe quel type de compte"),

    ('comptes.Adherent', 'adherent_detail', Acl.LIMITE, "Peut afficher ses propres infos"),
    ('comptes.Adherent', 'adherent_detail', Acl.BASIQUE, "Peut consulter les informations générales d'un adhérent"),
    ('comptes.Adherent', 'adherent_detail', Acl.ETENDU, "Peut consulter les informations personnelles d'un adhérent"),
    ('comptes.Adherent', 'adherent_detail', Acl.TOTAL, "Peut consulter toutes les informations d'un adhérent"),

    ('comptes.Adherent', 'adherent_modifier', Acl.LIMITE, "Peut modifier ses propres infos"),
    ('comptes.Adherent', 'adherent_modifier', Acl.BASIQUE, "Peut modifier les informations générales d'un adhérent"),
    ('comptes.Adherent', 'adherent_modifier', Acl.ETENDU, "Peut modifier les informations personnelles d'un adhérent"),
    ('comptes.Adherent', 'adherent_modifier', Acl.TOTAL, "Peut modifier toutes les informations d'un adhérent"),

    ('comptes.Adherent', 'adherent_chercher', Acl.BASIQUE, "Peut effectuer une recherche rapide parmi les adhérents"),
    ('comptes.Adherent', 'adherent_chercher', Acl.ETENDU, "Peut effectuer une recherche avancée sur les infos personnelles parmi les adhérents"),
    ('comptes.Adherent', 'adherent_chercher', Acl.TOTAL, "Peut chercher parmi tous les adhérents et anciens adhérents"),

    ('comptes.Adherent', 'adherent_change_pw', Acl.LIMITE, "Peut changer son mot de passe"),
    ('comptes.Adherent', 'adherent_change_pw', Acl.BASIQUE, "Peut changer le mot de passe d'une personne"),
    ('comptes.Adherent', 'adherent_change_pw', Acl.TOTAL, "Peut changer le mot de passe de tous les types de compte"),

    ('comptes.Adherent', 'adherent_desactiver', Acl.BASIQUE, "Peut activer/désactiver les comptes de type personne"),
    ('comptes.Adherent', 'adherent_desactiver', Acl.ETENDU, "Peut activer/désactiver les comptes de type personne, club/section"),
    ('comptes.Adherent', 'adherent_desactiver', Acl.TOTAL, "Peut activer/désactiver tous les types de comptes"),

    ('comptes.Adherent', 'adherent_supprimer', Acl.BASIQUE, "Peut supprimer les comptes de type personne"),
    ('comptes.Adherent', 'adherent_supprimer', Acl.ETENDU, "Peut supprimer tous les types de comptes"),
    ('comptes.Adherent', 'adherent_supprimer', Acl.TOTAL, "Peut supprimer et anonymiser tous les types de comptes"),

## Accréditations sur les aliases
    ('comptes.Alias', 'alias_adopter', Acl.LIMITE, "Peut adopter des aliases"),
    ('comptes.Alias', 'alias_adopter', Acl.TOTAL, "Peut assigner des aliases"),

    ('comptes.Alias', 'alias_abandonner', Acl.LIMITE, "Peut abandonner un de ses aliases"),
    ('comptes.Alias', 'alias_abandonner', Acl.TOTAL, "Peut retirer des aliases"),

## Accréditations sur les activités
    ('activites.Activite', 'activite_ajouter', Acl.LIMITE, "Peut proposer une activité en son propre nom"),
    ('activites.Activite', 'activite_ajouter', Acl.TOTAL, "Peut proposer une activité au nom de n'importe qui"),

    ('activites.Activite', 'activite_modifier', Acl.LIMITE, "Peut modifier une activité non validée organisée en son nom"),
    ('activites.Activite', 'activite_modifier', Acl.TOTAL, "Peut modifier n'importe quelle activité"),

    ('activites.Activite', 'activite_gerer', Acl.BASIQUE, "Peut rechercher une activité sans restriction"),
    ('activites.Activite', 'activite_gerer', Acl.ETENDU, "Peut rechercher, valider/dévalider une activité"),
    ('activites.Activite', 'activite_gerer', Acl.TOTAL, "Peut rechercher, valider/dévalider une activité et imprimer les listes d'invitations"),

## Accréditations sur les invités
    ('activites.Invite', 'invite_inviter', Acl.LIMITE, "Peut inviter quelqu'un à une activité"),
    ('activites.Invite', 'invite_inviter', Acl.TOTAL, "Peut assigner un invité à n'importe qui"),

    ('activites.Invite', 'invite_supprimer', Acl.LIMITE, "Peut retirer une de ses invitations"),
    ('activites.Invite', 'invite_supprimer', Acl.TOTAL, "Peut retirer n'importe quelle invitation"),

## Accréditations sur les notes
    ('consos.Note', 'note_ouvrir', Acl.BASIQUE, "Peut ouvrir une note à une personne, un club ou une section"),

    ('consos.Note', 'note_soft_lock', Acl.LIMITE, "Peut protéger sa propre note"),
    ('consos.Note', 'note_soft_lock', Acl.TOTAL, "Peut protéger toutes les notes"),

    ('consos.Note', 'note_hard_lock', Acl.BASIQUE, "Peut bloquer la note d'une personne"),
    ('consos.Note', 'note_hard_lock', Acl.TOTAL, "Peut bloquer toutes les notes"),

## Accréditations sur les transactions
    ('consos.Transaction', 'transaction_effectuer', Acl.LIMITE, "Peut effectuer des transferts depuis sa propre note"),
    ('consos.Transaction', 'transaction_effectuer', Acl.TOTAL, "Peut noter tous les types de transactions"),

    ('consos.Transaction', 'transaction_devalider', Acl.BASIQUE, "Peut valider/dévalider des transactions"),

## Accréditations sur les boutons
    ('consos.Bouton', 'bouton_ajouter', Acl.LIMITE, "Clubs/Sections uniquement. Peut ajouter un bouton pointant vers sa propre note"),
    ('consos.Bouton', 'bouton_ajouter', Acl.BASIQUE, "Peut ajouter un bouton vers un club/une section"),
    ('consos.Bouton', 'bouton_ajouter', Acl.TOTAL, "Peut ajouter un bouton vers n'importe qui"),

    ('consos.Bouton', 'bouton_modifier', Acl.LIMITE, "Clubs/Sections uniquement. Peut modifier un bouton pointant vers sa propre note"),
    ('consos.Bouton', 'bouton_modifier', Acl.BASIQUE, "Peut modifier les données d'un bouton sauf le bénéficiaire"),
    ('consos.Bouton', 'bouton_modifier', Acl.TOTAL, "Peut modifier toutes les données d'un bouton"),

    ('consos.Bouton', 'bouton_supprimer', Acl.LIMITE, "Clubs/Sections uniquement. Peut supprimer un bouton pointant vers sa propre note"),
    ('consos.Bouton', 'bouton_supprimer', Acl.BASIQUE, "Peut supprimer un bouton"),

    ('consos.Bouton', 'bouton_activer', Acl.BASIQUE, "Peut activer/désactiver un bouton"),
]

## Accréditations par défaut assignées aux nouveaux adhérents

from note_kfet.droits import Acl

ACCREDITATIONS_DEFAUT = {
    'personne' : [
        ('adherent_detail', Acl.LIMITE, False),
        ('adherent_change_pw', Acl.LIMITE, False),
        ('alias_adopter', Acl.LIMITE, False),
        ('alias_abandonner', Acl.LIMITE, False),
        ('activite_ajouter', Acl.LIMITE, False),
        ('activite_modifier', Acl.LIMITE, False),
        ('invite_inviter', Acl.LIMITE, False),
        ('invite_supprimer', Acl.LIMITE, False),
        ('note_soft_lock', Acl.LIMITE, False),
        ('transaction_effectuer', Acl.LIMITE, False),
    ],
    'club' : [
        ('adherent_detail', Acl.LIMITE, False),
        ('adherent_change_pw', Acl.LIMITE, False),
        ('alias_adopter', Acl.LIMITE, False),
        ('alias_abandonner', Acl.LIMITE, False),
        ('activite_ajouter', Acl.LIMITE, False),
        ('activite_modifier', Acl.LIMITE, False),
        ('note_soft_lock', Acl.LIMITE, False),
        ('transaction_effectuer', Acl.LIMITE, False),
    ],
    'section' : [
        ('adherent_detail', Acl.LIMITE, False),
        ('adherent_change_pw', Acl.LIMITE, False),
        ('alias_adopter', Acl.LIMITE, False),
        ('alias_abandonner', Acl.LIMITE, False),
        ('activite_ajouter', Acl.LIMITE, False),
        ('activite_modifier', Acl.LIMITE, False),
        ('note_soft_lock', Acl.LIMITE, False),
        ('transaction_effectuer', Acl.LIMITE, False),
    ],
    'débit' : [
        ('transaction_effectuer', Acl.TOTAL, False),
        ('transaction_devalider', Acl.BASIQUE, False),
    ],
}

################################################################################
#                                   Actions                                    #
################################################################################

#   (< Action >, < Description >)
ACTIONS = [
    ("crédit", "Créditer la note d'un adhérent"),
    ("retrait", "Retirer de l'argent à un adhérent"),
    ("transfert", "Transfert d'argent entre adhérents"),
    ("vente", "Vente d'un produit à un adhérent"),
    ("adhésion", "Prélèvement d'une cotisation"),
]

################################################################################
#                              Moyens de paiement                              #
################################################################################

#   (< Moyen >, < Description >)
PAIEMENTS = [
    ("note", "Flux entre notes"),
    ("espèces", "Espèces"),
    ("chèque", "Chèque"),
    ("virement", "Virement bancaire"),
    ("cb", "Carte Bleue"),
]

################################################################################
#                                   Catégories                                 #
################################################################################

#   (< Nom >, < Description >)
CATEGORIES = [
    ("gestion", "Opérations de gestion des notes"),
    ("alcool", "Boissons alcoolisées"),
    ("bouffe", "Denrées alimentaires"),
    ("soft", "Boissons non alcoolisées"),
    ("autre", "Autres consommables"),
]
