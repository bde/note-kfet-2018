"""
    Ajouts à l'environnement Jinja2 utilisé pour les templates.
"""

from django.urls import reverse

from django.utils import timezone

from django.contrib.staticfiles.storage import staticfiles_storage

from django.contrib.messages.api import get_messages
from django.contrib.messages.constants import DEFAULT_LEVELS

from django.template.defaultfilters import yesno, date, time

from django.conf import settings

from jinja2 import Environment

from note_kfet.droits import Acl


def environment(**options):
    env = Environment(**options)
    env.globals.update({
        'static': staticfiles_storage.url,
        'url': reverse,
        'now': timezone.now,
        'localtime': timezone.localtime,
        'messages': get_messages,
        'DEFAULT_MESSAGE_LEVELS': DEFAULT_LEVELS,
        'SITE_DOMAIN': settings.SITE_DOMAIN,
        'Acl' : Acl,
    })
    env.filters.update({
        'yesno' : yesno,
        'date' : date,
        'time' : time,
    })

    return env
