"""
    Vues spéciales
"""

from django.conf import settings
from django.http.response import HttpResponse
from django.views.generic.base import View
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

### Vue pour servir les images

class NginxMediaView(LoginRequiredMixin, View):
    """
        Vue permettant de servir les images en vérifiant les droits des adhérents et
        en utilisant la vitesse du reverse proxy.
    """
    def get(self, request, *args, **kwargs):
        from PIL import Image
        response = HttpResponse()
        media_type = kwargs.get('type', '')
        media_id = kwargs.get('id', '')
        media = Image.open("%s/%s/%s" % (settings.MEDIA_ROOT, media_type, media_id))
        response["Content-Type"] = Image.MIME.get(media.format)
        response["X-Accel-Redirect"] = "/internal/%s/%s" % (media_type, media_id)
        return response
