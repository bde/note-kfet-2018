"""
    Décorateurs utilisés par les vues de la Note Kfet 2018
"""

from functools import wraps

from rest_framework import status
from rest_framework.response import Response

from note_kfet.droits import D, Acl

def requires(perm, acl=Acl.LIMITE, detail=None):
    """
        Vérifie que l'utilisateur a bien le droit et l'accréditation
        demandée pour exécuter l'appel à l'API, et renvoie un code
        HTTP 403 dans le cas contraire.
    """
    def method_decorator(method):
        @wraps(method)
        def method_wrapper(self, request, *args, **kwargs):
            response = {} if detail is None else {"detail" : detail}
            if callable(perm) and not perm(request.user):
                return Response(response, status=status.HTTP_403_FORBIDDEN)

            if isinstance(perm, D):
                perm_args = (perm,)
            else:
                perm_args = (perm, acl,)

            if not request.user.has_perm(*perm_args):
                return Response(response, status=status.HTTP_403_FORBIDDEN)
            return method(self, request, *args, **kwargs)

        return method_wrapper

    return method_decorator
