"""
    Mixins pour les vues de la Note Kfet.
"""
from collections import OrderedDict

from django.forms import BaseFormSet

class NoteMixin(object):
    """
        Mixin ajoutant des informations au contexte utilisé pour le rendu des
        templates de la Note Kfet:
        *   Application de rattachement de la vue
        *   Classes CSS pour les widgets des formulaires (Forms/FilterSets)
    """
    app_label = None

    def get_form(self, **kwargs):
        form = super().get_form(**kwargs)
        return self._get_enhanced_form(form)

    def get_filterset(self, filterset_class):
        filterset = super().get_filterset(filterset_class)
        self._get_enhanced_form(filterset.form)
        return filterset

    def _get_enhanced_form(self, form):
        """
            Ajoute certaines classes à un formulaire pour améliorer
            les rendu dans les templates.
        """
        if isinstance(form, BaseFormSet):
            pass
        else:
            for field in form:
                if getattr(field.field.widget, 'input_type', None) == 'checkbox':
                    classes = 'form-check'
                else:
                    classes = 'form-control'
                if field.errors:
                    classes += ' form-control-danger'

                field.field.widget.attrs.update({'class' : classes})
                field.field.widget.attrs.update({'aria-describedby' :'help-%s' % field.html_name})

        return form

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(**{
            'app_label' : self.app_label,
            'onglets' : OrderedDict([
                (
                    'consos',
                    [
                        ('Consos', 'consos:consos'),
                        ('Crédits', 'consos:créditer'),
                        ('Retraits', 'consos:retirer'),
                        ('Transferts', 'consos:transférer'),
                        ('Boutons', 'consos:bouton_gestion'),
                    ],
                ),
                (
                    'comptes',
                    [
                        ('Inscription', 'comptes:inscription'),
                        ('Recherche', 'comptes:recherche'),
                    ],
                ),
                (
                    'activités',
                    [
                        ('Liste des activités', 'activités:accueil'),
                        ('Nouvelle activité', 'activités:ajout'),
                        ('Recherche', 'activités:recherche'),
                    ],
                ),
            ]),
        })

        return context
