"""
    Backends utilisés avec la note pour la gestion des droits et des méta-droits
"""

import re

from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend
from django.contrib.contenttypes.models import ContentType

from note_kfet.droits import Acl, D

ID_AUTH_REGEX = re.compile(r'^#([0-9]+)')

UserModel = get_user_model()

class NoteBackend(ModelBackend):
    """
        Un ModelBackend amélioré pour les besoins de la note, en
        particulier pour implémenter la gestion des accréditations
    """
    def authenticate(self, request, username=None, password=None, **kwargs):
        """
            Permet d'authentifier un utilisateur à l'aide de son ID BdE
        """
        m = ID_AUTH_REGEX.match(username or kwargs.get(UserModel.USERNAME_FIELD, ''))
        if m is not None:
            try:
                user = UserModel.objects.get(id=int(m.group(1)))
                username = getattr(user, UserModel.USERNAME_FIELD)
            except UserModel.DoesNotExist:
                UserModel().set_password(password)
                return None

        return super().authenticate(request, username=username, password=password, **kwargs)

    def _get_user_permissions(self, user_obj):
        return set(user_obj.accreditations.all())

    def _get_group_permissions(self, user_obj):
        # XXX : Pour le moment, la note n'utilise pas la notion de groupes
        #       donc on renvoie un EmptyQuerySet pour les accréditations de groupe
        return set(user_obj.accreditations.none())

    def _get_permissions(self, user_obj, obj, from_name):
        """
            Renvoie la liste des couples (droit, meta) associés à
            l'utilisateur user_obj.
        """
        perm_cache_name = '_%s_perm_cache' % from_name
        if not hasattr(user_obj, perm_cache_name):
            if user_obj.is_superuser:
                Droit = ContentType.objects.get(app_label="comptes", model="droit").model_class()
                Accreditation = ContentType.objects.get(app_label="comptes", model="accreditation").model_class()
                # Pour un superutilisateur, on récupère le droit le plus élevé pour chaque nom
                # de code.
                # Le QuerySet suivant donne le bon résultat seulement avec PostgreSQL
                # https://www.postgresql.org/docs/9.6/static/sql-select.html#SQL-DISTINCT
                droits = (
                    Droit.objects
                    .distinct('codename')
                    .order_by('codename', '-niveau')
                    .select_related('content_type')
                )
                accreds = {
                    Accreditation(
                        id=i, droit=d, adherent=user_obj, meta=True, dont_save=True,
                    ) for i, d in enumerate(droits)
                }
            else:
                # Pour un utilisateur normal, on récupère seulement les droits pour lesquels
                # il est accrédité
                accreds = getattr(self, '_get_%s_permissions' % from_name)(user_obj)
            setattr(user_obj, perm_cache_name, accreds)
        return getattr(user_obj, perm_cache_name)

    def has_perm(self, user_obj, perm, obj=None):
        """
            Renvoie True si l'utilisateur a l'accréditation demandée, False sinon.
        """
        if not user_obj.is_active:
            return False

        if isinstance(perm, str):
            perm = D(perm, *obj)
        elif not isinstance(perm, D):
            raise TypeError("perm doit être soit un objet D, soit un str")

        return perm.evaluate(self.get_all_permissions(user_obj))

    def has_module_perms(self, user_obj, app_label):
        """
            Renvoie True si l'utilisateur a une accréditation concernant l'app
            renseignée.
        """
        if not user_obj.is_active:
            return False
        for droit, _ in self.get_all_permissions(user_obj):
            if droit.content_type.app_label == app_label:
                return True
        return False
