"""
    Définition des droits et de l'objet D.

    Permet de brancher la note sur le système de droit de Django.
"""
from enum import IntEnum
from collections.abc import Generator

from django.utils import tree

################################################################################
#                   Définition de la sémantique des ACLs                       #
################################################################################

class Acl(IntEnum):
    """
        Les ACLs qui peuvent être appliqués à un droit.
        Leur ordre et surtout la valeur qui leur est assignée est capitale,
        puisqu'ils sont hiérarchisés.

        La sémantique des ACLs pour un droit est définie dans
        une carte d'accréditation (ACL_MAP).
    """
    LIMITE = 0  # Accréditation la plus faible
    BASIQUE = 1
    ETENDU = 2
    TOTAL = 255 # Accréditation la plus élevée

    @classmethod
    def to_choices(cls):
        return [(x.value, x.name) for x in list(cls)]

################################################################################
#                           Définition des objets Droit                        #
################################################################################

def _evaluate(droit, perm_list):
    """
        Détermine si l'accréditation représentée par le tuple (droit, ACL, meta)
        donné est présent dans la liste perm_list.
    """
    # Si on travaille sur un objet D, on évalue récursivement la fonction
    if isinstance(droit, D):
        return droit.evaluate(perm_list)

    # On travaille sur une simple feuille (codename, acl, meta)
    # et une liste d'objets de la même forme
    if len(droit) == 2:
        droit += (False,)

    for accred in perm_list:
        full_codename = "%s.%s" % (accred.droit.content_type.app_label, accred.droit.codename)
        if (full_codename == droit[0]
                and accred.droit.niveau >= droit[1]
                and (accred.meta or not droit[2])):
            return True
    return False

class D(tree.Node):
    """
        Amélioration du système de droits avec la possibilité d'utiliser
        des connecteurs logiques.

        Dérive de django.utils.tree.Node et est implémenté selon la même
        logique que les objets django.db.models.Q.
    """
    # Connecteurs logiques
    AND = 'AND'
    OR = 'OR'
    default = AND

    def __init__(self, *args, **kwargs):
        """
            Enrobe un ensemble (droit, ACL, meta) dans un objet D, lui permettant
            d'être associé à d'autres ensembles de droits.
        """
        if len(args) == 0:
            children = []
        elif isinstance(args[0], D):
            children = [args[0]]
        elif isinstance(args[0], (list, set, tuple, Generator)):
            children = list(args)
        elif len(args) == 2 and isinstance(args[0], str) and isinstance(args[1], int):
            args = list(args)
            args.append(False)
            children = [args]
        elif len(args) >= 3 and isinstance(args[0], str) and isinstance(args[1], int) and isinstance(args[2], bool):
            children = [args[:3]]
        else:
            raise ValueError(
                "Un objet D ne peut être initialisé qu'avec un 2-tuple "
                "(codename, acl), un 3-tuple (codename, acl, meta) ou un itérable "
                "contenant des objets de ce type"
            )
        super().__init__(children=children)

    def _combine(self, other, connector):
        """
            Combine l'élément avec un autre élément D avec le connecteur
            spécifié.
        """
        if not isinstance(other, D):
            raise TypeError(other)

        obj = type(self)()
        obj.connector = connector
        obj.add(self, connector)
        obj.add(other, connector)
        return obj

    def __or__(self, other):
        return self._combine(other, self.OR)

    def __and__(self, other):
        return self._combine(other, self.AND)

    def __invert__(self):
        obj = type(self)()
        obj.add(self, self.AND)
        obj.negate()
        return obj

    def evaluate(self, perm_list=None):
        """
            Évalue l'ensemble de droit représenté par l'élément relativement à
            perm_list.
        """
        if perm_list is None:
            perm_list = set()

        if not self:
            # On manipule un noeud vide
            return False
        elif not self.children:
            # On manipule une feuille
            return _evaluate(self, perm_list)
        elif self.negated:
            # On manipule un noeud NOT
            child = self.children[0]
            return not child.evaluate(perm_list)
        elif self.connector == self.AND:
            # On manipule un noeud AND
            return all([_evaluate(x, perm_list) for x in self.children])
        elif self.connector == self.OR:
            # On manipule un noeud OR
            return any([_evaluate(x, perm_list) for x in self.children])
        else:
            # Le connecteur n'est pas pris en charge
            raise NotImplementedError
