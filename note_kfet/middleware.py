"""
    Middlewares utilisés par la NK2018
"""

from django.conf import settings

class NKSessionExpiryMiddleware:
    """
        Middleware utilisé par la NK2018 pour limiter la durée d'une
        session d'un utilisateur authentifié qui resterait inactif
        trop longtemps.
    """
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.user.is_authenticated:
            request.session.set_expiry(settings.SESSION_COOKIE_AGE)
        return self.get_response(request)
