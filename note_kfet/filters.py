"""
    Filtres spéciaux pour la Note Kfet
"""

import operator
from functools import reduce
from distutils.util import strtobool

from django.db import models
from django.db.models.constants import LOOKUP_SEP

from rest_framework import filters
from rest_framework.compat import distinct

from note_kfet.utils import escape_regex, to_bool

class NoteSearchFilter(filters.SearchFilter):
    """
        Filtre de recherche permettant d'effectuer au choix
        une recherche brute ou une recherche par expression régulière
        en commençant à chercher au début d'un mot
    """
    def construct_search(self, field_name):
        return LOOKUP_SEP.join([field_name, 'iregex'])

    def get_search_terms(self, request):
        use_regex_mode = to_bool(request.query_params.get('regex', 'false'))
        return [
            "^%s" % (
                search_term if use_regex_mode else escape_regex(search_term)
            ) for search_term in super().get_search_terms(request)
        ]
