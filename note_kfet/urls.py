"""note_kfet URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.staticfiles import views
from django.views.generic.base import RedirectView

from note_kfet.views import NginxMediaView

urlpatterns = [
    # Page d'accueil
    url(r'^$', RedirectView.as_view(permanent=True, pattern_name="comptes:login"), name="accueil"),
    # Applications
    url(r'^comptes/', include(('comptes.urls', 'comptes'), namespace="comptes")),
    url(r'^consos/', include(('consos.urls', 'consos'), namespace="consos")),
    url(r'^activites/', include(('activites.urls', 'activites'), namespace="activités")),
    # API
    url(r'^api/', include(('note_kfet.api', 'api'), namespace="api")),
    # URL de fichiers multimédias, accessibles après autorisation
    url(r'^media/(?P<type>[a-zA-Z0-9]+)/(?P<id>[a-f0-9]{8}(-[a-f0-9]{4}){3}-[a-f0-9]{12})/', NginxMediaView.as_view(), name="nginx-media"),
]

if settings.DEBUG:
    urlpatterns += [
        url(r'^static/(?P<path>.*)', views.serve),
    ]
