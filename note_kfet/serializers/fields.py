#-*- coding: utf-8 -*-

from django.db import models

from rest_framework import serializers

from note_kfet.serializers import mixins


__all__ = ['Base64ImageField']


class Base64ImageField(mixins.Base64FieldMixin, serializers.ImageField):
    """
        Champ permettant d'encoder/décoder une image en base 64
    """
    def get_file_type(self, obj):
        from PIL import Image
        im = Image.open(obj.path)
        return Image.MIME.get(im.format)

# Par défaut, on veut pouvoir communiquer les fichiers en base 64, donc on
# override le champ de sérialisation par défaut
serializers.ModelSerializer.serializer_field_mapping[models.ImageField] = Base64ImageField
