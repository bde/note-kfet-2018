#-*- coding: utf-8 -*-

import uuid
import base64
import binascii

from django.core.files.base import ContentFile
from django.core.validators import EMPTY_VALUES

from rest_framework.exceptions import ValidationError


__all__ = ['Base64FieldMixin', 'DynamicFieldsMixin']

class DynamicFieldsMixin(object):
    """
        Un mixin permettant de modifier de manière dynamique les champs
        utilisés dans un sérialiseur.
    """
    def __init__(self, *args, **kwargs):
        if getattr(self.__class__.Meta, 'default_empty', True):
            default_fields = []
        else:
            default_fields = self.__class__.Meta.fields

        fields = set(kwargs.pop('fields', default_fields))
        super().__init__(*args, **kwargs)
        for field in set(self.fields) - fields:
            self.fields.pop(field)

class Base64FieldMixin(object):
    """
        Un mixin permettant de sérialiser et désérialiser un fichier en base 64
        Adapté de DRF Extra Fields (https://github.com/Hipo/drf-extra-fields/)
    """
    def to_representation(self, obj):
        """
            Renvoie la représentation en base 64 de l'objet donné en paramètre
        """
        try:
            with open(obj.path, mode='rb') as f:
                return "%s;base64,%s" % (
                    self.get_file_type(obj),
                    base64.b64encode(f.read()).decode(),
                )
        except ValueError:
            # Pas de fichier correspondant
            return None
        except Exception:
            raise IOError("Erreur lors de l'encodage des données")

    def to_internal_value(self, b64_data):
        """
            Renvoie un fichier en mémoire obtenu en décodant les données reçues
            encodées en base 64
        """
        if b64_data in EMPTY_VALUES:
            return None

        if not isinstance(b64_data, str):
            raise ValidationError("Les données encodées ne sont pas sous forme d'une chaîne en base 64")

        if ";base64," in b64_data:
            b64_data = b64_data.split(';base64,')[-1]

        try:
            data = base64.b64decode(b64_data)
        except (TypeError, binascii.Error, IOError, ValueError):
            raise ValidationError("Données encodées en base 64 invalides")

        file_obj = ContentFile(data, name=str(uuid.uuid4()))
        return super().to_internal_value(file_obj)

    def get_file_type(self, obj):
        raise NotImplementedError
