#!/usr/bin/env python3

"""
    Mise en place des triggers sur la base de données
"""

import sys
import os
import hashlib

import psycopg2

# Connexion à la base de données note_kfet
conn = psycopg2.connect(user="postgres", dbname="note_kfet")
cur = conn.cursor()

# Tables concernée par les triggers
tables = [
    # Comptes
    {'table_name' : 'comptes_adherent', 'when' : 'BEFORE', 'level' : 'ROW', 'event' : 'DELETE'},
    {'table_name' : 'comptes_adherent', 'when' : 'BEFORE', 'level' : 'STATEMENT', 'event' : 'TRUNCATE'},
    {'table_name' : 'comptes_alias', 'when' : 'BEFORE', 'level' : 'ROW', 'event' : 'DELETE'},
    {'table_name' : 'comptes_alias', 'when' : 'BEFORE', 'level' : 'STATEMENT', 'event' : 'TRUNCATE'},
    {'table_name' : 'comptes_historique', 'when' : 'BEFORE', 'level' : 'ROW', 'event' : 'UPDATE OR DELETE'},
    {'table_name' : 'comptes_historique', 'when' : 'BEFORE', 'level' : 'STATEMENT', 'event' : 'TRUNCATE'},
    # Consos
    {'table_name' : 'consos_transaction', 'when' : 'BEFORE', 'level' : 'ROW', 'event' : 'DELETE'},
    {'table_name' : 'consos_transaction', 'when' : 'BEFORE', 'level' : 'STATEMENT', 'event' : 'TRUNCATE'},
]

# Activation de l'extension PL/Python 3
cur.execute("""CREATE EXTENSION IF NOT EXISTS plpython3u;""")

# Écriture des triggers
cur.execute("""
    CREATE OR REPLACE FUNCTION annule_operation()
    RETURNS trigger
    LANGUAGE plpython3u
    AS $$
    if TD["event"] == "INSERT":
        plpy.error("Opération annulée", detail="Impossible d'insérer des données dans la table", table_name=TD["table_name"])
    elif TD["event"] == "UPDATE":
        plpy.error("Opération annulée", detail="Impossible de modifier une entrée dans la table", table_name=TD["table_name"])
    elif TD["event"] == "DELETE":
        plpy.error("Opération annulée", detail="Impossible de supprimer une entrée dans la table", table_name=TD["table_name"])
    elif TD["event"] == "TRUNCATE":
        plpy.error("Opération annulée", detail="Impossible de vider la table", table_name=TD["table_name"])

    return "SKIP" if TD["level"] == "ROW" else None
    $$;
""")

# Mise en place des triggers
for table in tables:
    md5_hash = hashlib.md5(
        (table['table_name'] + table['when'] + table['level'] + table['event']).encode('utf-8')
    ).hexdigest()
    table.update({
        'md5' : md5_hash,
    })
    cur.execute("""
    CREATE TRIGGER preserve_%(table_name)s_%(md5)s %(when)s %(event)s
    ON %(table_name)s
    FOR EACH %(level)s
    EXECUTE PROCEDURE annule_operation();
    """ % table)

conn.commit()
