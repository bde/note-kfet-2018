"""
    Signaux utilisés pour interagir avec les modèles de l'application Comptes.
"""

import os

from django.conf import settings
from django.db.models import signals
from django.dispatch import receiver

from PIL import Image

from comptes.models import Adherent

@receiver(signals.post_save, sender=Adherent)
def send_mail_after_registration(sender, instance=None, created=False, **kwargs):
    """
        Envoie un mail de bienvenue à l'adhérent après qu'il ait été créé.
    """
    if created:
        instance.email_user_from_template('Activation de ta note Kfet', 'adherent_bienvenue')

@receiver(signals.pre_save, sender=Adherent)
def delete_old_avatar_if_changed(sender, instance=None, **kwargs):
    """
        Supprime l'ancien avatar d'un adhérent
    """
    if instance.id is None:
        return None

    old_avatar = Adherent.objects.only("avatar").get(id=instance.id).avatar
    if not old_avatar:
        return None

    if instance.avatar != old_avatar and os.path.isfile(old_avatar.path):
        os.remove(old_avatar.path)

@receiver(signals.post_save, sender=Adherent)
def resize_avatar_if_necessary(sender, instance=None, **kwargs):
    """
        Modifie la taille de l'avatar après son envoi sur le serveur
    """
    if not instance.avatar:
        return None

    if (instance.avatar.width > settings.THUMBNAIL_SIZE[1]
            or instance.avatar.height > settings.THUMBNAIL_SIZE[0]):
        image = Image.open(instance.avatar.path)
        image.thumbnail(settings.THUMBNAIL_SIZE)
        image.save(instance.avatar.path, "PNG")

@receiver(signals.post_delete, sender=Adherent)
def send_mail_after_deletion(sender, instance=None, email=None, nom=None, prenom=None, shred=False, **kwargs):
    """
        Envoie un mail pour confirmer la suppression d'un compte à un adhérent.
    """
    context = {
        'nom' : nom,
        'prenom' : prenom,
        'shred' : shred,
    }
    instance.email = email
    instance.email_user_from_template('Suppression de ton compte Note Kfet', 'adherent_suppression', context=context)
