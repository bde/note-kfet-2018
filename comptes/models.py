"""
    Modèles de l'application «Comptes».
"""

import uuid
from datetime import timedelta

from django.db import models, transaction, IntegrityError
from django.db.models import Q
from django.db.models.signals import post_delete
from django.contrib.auth import get_backends
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.core.mail import send_mail
from django.core.exceptions import ValidationError, PermissionDenied
from django.template import loader
from django.urls import reverse
from django.utils import timezone

from note_kfet import droits as m_droits
from note_kfet.utils import CumulativeDict
from note_kfet.environnement import ACCREDITATIONS_DEFAUT

from comptes import validators

class Section(models.Model):
    """
        Une section regroupant plusieurs adhérents.

        -   sigle   [varchar(255)]  -> Sigle identifiant de manière unique une section
        -   nom     [varchar]       -> Nom de la section
        -   ferme   [boolean]       -> La section admet-elle de nouveau membres ?
    """
    sigle = models.CharField(
        "sigle",
        max_length=255,
        unique=True,
        blank=False,
        null=False,
    )
    nom = models.TextField(
        "nom",
        blank=False,
        null=False,
    )
    ferme = models.BooleanField(
        "fermé",
        default=False,
    )
    class Meta:
        default_permissions = []

    def __str__(self):
        return "%s (%s)" % (self.nom, self.sigle)

class Alias(models.Model):
    """
        Un alias désignant un adhérent.

        -   alias               [varchar]   -> Alias de l'adhérent
        -   proprietaire[0-1]   [int*]      -> Propriétaire de l'alias
    """
    alias = models.TextField(
        "alias",
        unique=True,
        blank=False,
        null=False,
        validators=[
            validators.DeletedValidator(inverse_match=True),
            validators.IDValidator(inverse_match=True),
            validators.PseudoValidator(),
        ]
    )
    proprietaire = models.ForeignKey(
        'comptes.Adherent',
        models.CASCADE,
        verbose_name="propriétaire",
        related_name="aliases",
        blank=False,
        null=True,
        default=None,
    )
    class Meta:
        default_permissions = []
        permissions = [
            ('alias_adopter', 'Adopter un alias'),
            ('alias_abandonner', 'Abandonner un alias'),
        ]

    def __str__(self):
        return "%s -> %s" % (
            self.alias,
            ("#%d" % self.proprietaire.id) if self.proprietaire else None,
        )

    @property
    def is_pseudo(self):
        """
            Indique si l'alias courant est le pseudo d'un adhérent.
        """
        if self.proprietaire is None or not self.proprietaire.pseudo == self.alias:
            return False
        else:
            return True

    @transaction.atomic
    def save(self, *args, **kwargs):
        """
            Enregistre un alias.
        """
        super().save(*args, **kwargs)
        Historique.objects.create(alias=self, suivant=self.proprietaire)

    @transaction.atomic
    def delete(self, *args, **kwargs):
        """
            Supprime un alias.

            Dans les faits, l'alias est simplement abandonné. Il peut ainsi
            être récupéré plus tard.
        """
        ancien_proprio = self.proprietaire
        self.proprietaire = None
        super().save(*args, **kwargs)
        Historique.objects.create(alias=self, precedent=ancien_proprio)

class Historique(models.Model):
    """
        Une entrée dans l'historique pour le suivi des pseudos.

        -   date            [tstz]  -> Date de la modification
        -   alias           [int*]  -> Alias suivi
        -   precedent[0-1]  [int*]  -> Ancien propriétaire
        -   suivant[0-1]    [int*]  -> Nouveau propriétaire
    """
    date = models.DateTimeField(
        auto_now_add=True,
        null=False,
    )
    alias = models.ForeignKey(
        'comptes.Alias',
        models.CASCADE,
        null=False,
    )
    precedent = models.ForeignKey(
        'comptes.Adherent',
        models.CASCADE,
        null=True,
        related_name="aliases_cedes",
    )
    suivant = models.ForeignKey(
        'comptes.Adherent',
        models.CASCADE,
        null=True,
        related_name="aliases_acquis",
    )
    class Meta:
        default_permissions = []

    def clean(self):
        """
            Vérification des contraintes d'intégrité
        """
        if self.precedent == self.suivant:
            raise ValidationError("Ligne d'historique invalide : precedent == suivant")

    def delete(self, *args, **kwargs):
        """
            Supprime une entrée de l'historique des pseudos.
            Pour le moment, aucune entrée n'est réellement supprimée.
        """
        # No-op
        raise IntegrityError("Il n'est pas possible de supprimer une entrée de l'historique")

    def __str__(self):
        return "Alias %s (%s -> %s)" % (self.alias.alias, self.precedent, self.suivant)

class Droit(models.Model):
    """
        Un droit de la Note Kfet.

        Chaque droit est un droit basique Django auquel on ajoute un niveau
        de confidentialité et une explication de ce dernier.

        Plusieurs droits peuvent partager le même nom de code s'ils offrent des
        possibilités similaires mais à des niveaux de confidentialité différents

        -   description     [str]       ->  Description du droit
        -   content_type    [int*]      ->  ContentType associé au modèle concerné par ce droit
        -   codename        [str]       ->  Nom de code
        -   niveau          [int]       ->  Niveau de confidentialité de ce Droit
    """
    description = models.CharField('description', max_length=255)
    content_type = models.ForeignKey(
        'contenttypes.ContentType',
        models.CASCADE,
        verbose_name='content type',
    )
    codename = models.CharField("nom de code", max_length=100)
    niveau = models.PositiveIntegerField(blank=False, null=False, choices=m_droits.Acl.to_choices())

    class Meta:
        default_permissions = []
        unique_together = [
            ('codename', 'niveau'),
        ]

    def __lt__(self, y):
        return self.niveau < y

    def __le__(self, y):
        return self.niveau <= y

    def __gt__(self, y):
        return self.niveau > y

    def __ge__(self, y):
        return self.niveau >= y

    def __str__(self):
        return self.description

class Accreditation(models.Model):
    """
        Accréditation qui peut être données à un adhérent pour un droit donné.

        Chaque accréditation peut porter la mention 'Méta' pour indiquer qu'il
        s'agit d'une méta-accréditation, i.e. qu'elle autorise son posseur
        à accréditer une autre personne avec ce droit.

        -   droit           [int*]      ->  Droit associé
        -   adherent        [int*]      ->  Adhérent accrédité
        -   meta            [boolean]   ->  Mention 'Méta'
    """
    droit = models.ForeignKey(
        'comptes.Droit',
        on_delete=models.CASCADE,
        related_name="accreditations",
        blank=False,
        null=False,
    )
    adherent = models.ForeignKey(
        'comptes.Adherent',
        models.CASCADE,
        verbose_name="adhérent",
        related_name="accreditations",
        blank=False,
        null=False,
    )
    meta = models.BooleanField(
        "méta-droit",
        default=False,
    )

    class Meta:
        default_permissions = []
        unique_together = [
            ('droit', 'adherent'),
        ]

    def __init__(self, *args, **kwargs):
        dont_save = kwargs.pop("dont_save", False)
        super().__init__(*args, **kwargs)
        self.__dont_save = dont_save

    def __str__(self):
        desc = "%s %s %s" % ("Méta" if self.meta else "", self.adherent.pseudo, self.droit)
        return desc.strip()

    def save(self, *args, **kwargs):
        """
            Enregistre une accréditation.

            Supprime toutes les accréditations de l'utilisateur avec le même
            nom de code avant d'enregistrer une nouvelle accréditation.
        """
        if self.__dont_save:
            raise RuntimeError("Cette instance d'accréditation ne doit pas être enregistrée")
        self.full_clean(exclude=['id'] if self.id is None else [])
        # Suppression de toutes les accréditations avec le même nom de code
        with transaction.atomic():
            qs = (
                Accreditation.objects
                .filter(adherent=self.adherent, droit__codename=self.droit.codename)
                .exclude(id=self.id)
            )
            if qs.exists():
                qs.delete()
            super().save(*args, **kwargs)

class Adhesion(models.Model):
    """
        Une adhésion liée à un adhérent

        -   debut       [tstz]  -> Date de début de l'adhésion
        -   fin         [tstz]  -> Date de fin de l'adhésion
        -   section     [int*]  -> Section de l'adhérent au moment de l'adhésion
        -   adherent    [int*]  -> Adhérent concerné par l'adhésion
    """
    debut = models.DateTimeField(
        "début",
        auto_now=False,
        auto_now_add=True,
        blank=True,
        null=False,
    )
    fin = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=False,
    )
    section = models.ForeignKey(
        'comptes.Section',
        models.CASCADE,
        related_name="adhesions",
        blank=True,
        null=True,
    )
    adherent = models.ForeignKey(
        'comptes.Adherent',
        models.CASCADE,
        verbose_name="adhérent",
        related_name="adhesions",
        blank=False,
        null=False,
    )

    class Meta:
        verbose_name = "adhésion"
        default_permissions = []

    def clean(self):
        """
            Vérification des contraintes sur les adhésions
        """
        if self.debut < self.fin:
            raise ValidationError("Date d'adhésion invalide : Elle finit avant d'avoir commencé")

        for adhesion in Adhesion.objects.filter(~Q(id=self.id) & Q(adherent=self.adherent)):
            if (adhesion.fin > self.debut) and (self.fin > adhesion.debut):
                raise ValidationError(
                    "Limite de l'adhésion invalide : "
                    "Chevauche une autre adhésion pour le même adhérent"
                )

    def __str__(self):
        return "(#%s, %s) %s -> %s" % (self.adherent, self.section, self.debut, self.fin)

class AdherentManager(BaseUserManager):
    """
        Manager pour la création d'adhérent
    """
    def create(self, *args, **kwargs):
        """
            Crée un utilisateur dans la base de données.
        """
        return self.create_user(*args, **kwargs)

    def create_user(self, pseudo=None, password=None, **kwargs):
        """
            Crée un utilisateur non privilégié dans la base de données
        """
        kwargs['is_superuser'] = False
        return self.__create(pseudo, password, **kwargs)

    def create_superuser(self, pseudo, password, **kwargs):
        """
            Crée un utilisateur privilégié dans la base de données
        """
        kwargs['is_superuser'] = True
        return self.__create(pseudo, password, **kwargs)

    def __create(self, pseudo, password, **kwargs):
        """
            Crée réelement un utilisateur dans la base de données.
        """
        # Vérification de la présence des champs requis
        if not set(self.model.REQUIRED_FIELDS) <= set(kwargs.keys()):
            raise ValueError(
                "Les champs suivants doivent être "
                "renseignés %s (%s donnés)" % (
                    ", ".join(self.model.REQUIRED_FIELDS),
                    ", ".join(kwargs.keys()),
                )
            )

        if pseudo is None:
            raise ValueError("Un pseudo doit être spécifié")

        if password is None:
            password = self.make_random_password()

        # On uniformise l'aspect des données
        kwargs['pseudo'] = self.model.normalize_username(pseudo)
        section = kwargs.pop('section','EXT')
        if not isinstance(section, Section):
            section = Section.objects.get(sigle=section)
        # Création d'un adhérent et de tous les objets associés en un bloc
        with transaction.atomic():
            new_adh = self.model(**kwargs)
            new_adh.set_password(password)
            new_adh.save()
            pseudo, _ = Alias.objects.update_or_create(
                defaults={'proprietaire': new_adh},
                alias=new_adh.pseudo,
                proprietaire=None,
            )
            Historique.objects.create(alias=pseudo, suivant=new_adh)
            Adhesion.objects.create(
                adherent=new_adh,
                fin=timezone.now() + timedelta(days=365),
                section=section,
            )
            for codename, niveau, meta in ACCREDITATIONS_DEFAUT.get(new_adh.type, []):
                perm = Accreditation.objects.create(
                    droit=Droit.objects.get(codename=codename, niveau=niveau),
                    adherent=new_adh,
                    meta=meta,
                )
        return new_adh

def upload_to_avatars(instance, _):
    """
        Renvoie le chemin relatif d'enregistrement des avatars d'un adhérent
    """
    return "avatars/%s" % instance.uuid

class Adherent(AbstractBaseUser):
    """
        Un adhérent de l'association.

        -   pseudo      [varchar(1023)] -> Pseudonyme de l'adhérent
        -   nom         [varchar(255)]  -> Nom de l'adhérent
        -   prenom      [varchar(255)]  -> Prénom de l'adhérent
        -   email       [varchar(254)]  -> Email de l'adhérent
        -   sexe[0-1]   [varchar(1)]    -> Sexe de l'adhérent
        -   avatar[0-1] [varchar(255)]  -> Avatar de l'adhérent
        -   type        [varchar(255)]  -> Type d'adhérent
        -   telephone   [varchar(255)]  -> Numéro de téléphone de l'adhérent
        -   adresse     [varchar]       -> Adresse postale de l'adhérent
        -   remunere    [boolean]       -> Indique si l'adhérent est rémunéré
        -   pbsante     [varchar]       -> Éventuel problème de santé
        -   remarque    [varchar]       -> Remarque sur l'adhérent
        -   uuid        [uuid]          -> Identifiant anonyme secondaire.
        -   supprime    [boolean]       -> Indique si le compte a été supprimé.
        -   droits[0-N] [droits*]       -> Droits attribués à l'adhérent

            =====  Couche de compatibilité Django  =====

        -   is_staff    [boolean]       -> L'adhérent est MA.
        -   is_active   [boolean]       -> Le compte est actif
        -   is_superuser[boolean]       -> Super-utilisateur
    """
    USERNAME_FIELD = 'pseudo'
    REQUIRED_FIELDS = ['nom', 'prenom', 'email', 'sexe', 'type']

    PERSONNE = 'personne'
    CLUB = 'club'
    SECTION = 'section'
    DEBIT = 'débit'

    TYPES = [
        (PERSONNE, "Personne"),
        (CLUB, "Club"),
        (SECTION, "Section"),
        (DEBIT, "Débit"),
    ]

    ND = None
    MASCULIN = "M"
    FEMININ = "F"

    SEXES = [
        (ND, "ND"),
        (MASCULIN, "M"),
        (FEMININ, "F"),
    ]

    CHAMPS_VISIBLES = CumulativeDict([
        (m_droits.Acl.BASIQUE, ['nom', 'prenom', 'sexe', 'email', 'is_active', 'supprime', 'adhesions', 'aliases']),
        (m_droits.Acl.ETENDU, ['telephone', 'adresse', 'remunere', 'remarque', 'accreditations', 'avatar']),
        (m_droits.Acl.TOTAL, ['pbsante', 'is_staff']),
    ])

    CHAMPS_MODIFIABLES = CumulativeDict([
        (m_droits.Acl.BASIQUE, ['nom', 'prenom', 'sexe', 'email',]),
        (m_droits.Acl.ETENDU, ['telephone', 'adresse', 'remunere', 'remarque', 'avatar']),
        (m_droits.Acl.TOTAL, ['pbsante',]),
    ])

    objects = AdherentManager()

    pseudo = models.CharField(
        max_length=1023,
        unique=True,
        blank=False,
        null=False,
        validators=[
            validators.IDValidator(inverse_match=True),
            validators.PseudoValidator(),
        ],
    )
    nom = models.CharField(
        max_length=255,
        blank=True,
        null=False,
    )
    prenom = models.CharField(
        "prénom",
        max_length=255,
        blank=True,
        null=False,
    )
    email = models.EmailField(
        blank=False,
        null=False,
    )
    sexe = models.CharField(
        max_length=1,
        blank=True,
        null=True,
        choices=SEXES,
        default=None,
    )
    type = models.CharField(
        max_length=255,
        blank=False,
        null=False,
        choices=TYPES,
        default=PERSONNE,
    )
    avatar = models.ImageField(
        max_length=255,
        upload_to=upload_to_avatars,
        blank=True,
        null=True,
        default=None,
    )
    telephone = models.CharField(
        verbose_name="téléphone",
        max_length=50,
        blank=True,
        null=False,
        validators=[validators.TelephoneValidator()],
    )
    adresse = models.TextField(
        blank=True,
        null=False,
        default='',
    )
    remunere = models.BooleanField(
        verbose_name="rémunéré",
        default=False,
    )
    pbsante = models.TextField(
        verbose_name="problème de santé",
        blank=True,
        null=False,
        default='',
    )
    remarque = models.TextField(
        blank=True,
        null=False,
        default='',
    )
    uuid = models.UUIDField(
        unique=True,
        null=False,
        default=uuid.uuid4,
    )
    is_staff = models.BooleanField(
        "membre actif",
        default=False,
    )
    is_active = models.BooleanField(
        "compte actif",
        default=True,
    )
    is_superuser = models.BooleanField(
        "superutilisateur",
        default=False
    )
    supprime = models.BooleanField(
        "compte supprimé",
        default=False,
    )
    droits = models.ManyToManyField(
        'comptes.Droit',
        through='comptes.Accreditation',
        through_fields=('adherent', 'droit'),
        related_name='adherents',
    )

    # Métadonnées

    class Meta:
        verbose_name = "adhérent"
        default_permissions = []
        permissions = [
            ('adherent_inscrire', "Inscrire un nouvel adhérent"),
            ('adherent_detail', "Consulter le profil d'un adhérent"),
            ('adherent_modifier', "Modifier le profil d'un adhérent"),
            ('adherent_chercher', "Chercher parmi les adhérents"),
            ('adherent_change_pw', "Changer un mot de passe"),
            ('adherent_desactiver', "Désactiver le compte d'un adhérent"),
            ('adherent_supprimer', "Supprimer un adhérent"),
        ]

    def __str__(self):
        return "#%d : %s (%s %s)" % (self.id, self.pseudo, self.prenom, self.nom)

    # Vérification des contraintes d'intégrité

    def clean(self):
        """
            Vérification des contraintes sur le modèle.
        """
        if (self.type == self.PERSONNE) and (not self.supprime) and (self.sexe is None):
            raise ValidationError("Le sexe doit être renseigné.")

        if ((self.pseudo.startswith("__deleted__") or (self.type != self.PERSONNE)) and
                (self.sexe is not None)):
            raise ValidationError("Le sexe ne peut pas être indiqué")

        if (self.type != self.PERSONNE) and self.is_superuser:
            raise ValidationError("Un adhérent superutilisateur doit être de type 'personne'")

        if (self.type != self.PERSONNE) and self.is_staff:
            raise ValidationError("Un adhérent M.A. doit être de type 'personne'")

    def save(self, *args, **kwargs):
        """
            Enregistre un adhérent dans la base de données
        """
        self.full_clean(exclude=['id'] if self.id is None else [])
        super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        """
            Supprime un adhérent.
            Dans les faits, celui-ci n'est jamais retiré de la base de données.
        """
        email = self.email
        shred = kwargs.get('shred', False)
        nom, prenom = self.nom, self.prenom

        if shred:
            self.shred()
        else:
            self.supprime = True

        self.save()
        post_delete.send(self.__class__, instance=self, shred=shred, nom=nom, prenom=prenom)

    def shred(self, *args, **kwargs):
        """
            Efface les données d'un adhérent dans la base de données.
            ATTENTION : L'entrée n'est jamais vraiment supprimée, les données
                        sont simplement effacées.
        """
        self.pseudo = "__deleted__%d" % self.id
        self.nom, self.prenom = "__deleted__%d" % self.id, "__deleted__%d" % self.id
        self.sexe = None
        self.email, self.adresse, self.telephone = "nobody@crans.org", "", ""
        self.pbsante, self.remarque = "", ""
        self.is_superuser, self.is_staff, self.is_active, self.remunere = [False] * 4
        self.droits.clear()
        for alias in self.aliases.all():
            alias.delete()

        self.supprime = True

    # Couche de compatibilité Django

    def _has_perm(self, perm, obj, meta):
        """
            Renvoie True si l'utilisateur a l'accréditation désignée par
            (perm, niveau, meta), et False sinon.
        """
        for backend in get_backends():
            if not hasattr(backend, 'has_perm'):
                continue
            try:
                if backend.has_perm(self, perm, (obj, meta)):
                    return True
            except PermissionDenied:
                return False
        return False

    def has_perm(self, perm, obj=m_droits.Acl.LIMITE):
        """
            Renvoie True si l'utilisateur a l'accréditation désignée par
            (perm, niveau), et False sinon.
            Ne tient pas compte de la mention 'méta'.
        """
        return self._has_perm(perm, obj, False)

    def has_meta(self, perm, obj=m_droits.Acl.LIMITE):
        """
            Renvoie True si l'utilisateur a l'accréditation désignée par
            (perm, niveau) avec une mention 'méta', False sinon.
        """
        return self._has_perm(perm, obj, True)

    def has_perms(self, perm_list, obj=None):
        if not isinstance(perm_list, (list, tuple, set,)):
            perm_list = [perm_list]
        return all(self.has_perm(x) for x in perm_list)

    def has_module_perms(self, app_label):
        """
            Renvoie True si l'utilisateur a une des accréditations définies pour
            l'application demandée.
        """
        for backend in get_backends():
            if not hasattr(backend, 'has_module_perms'):
                continue
            try:
                if backend.has_module_perms(self, app_label):
                    return True
            except PermissionDenied:
                return False
        return False

    def get_group_permissions(self, obj=None):
        """
            Renvoie toutes les accréditations liées aux groupes auxquels appartient
            l'adhérent.
        """
        permissions = set()
        for backend in get_backends():
            if hasattr(backend, "get_group_permissions"):
                permissions.update(backend.get_group_permissions(self, obj))
        return permissions

    def get_all_permissions(self, obj=None):
        """
            Renvoie toutes les accréditations de l'adhérent.
        """
        permissions = set()
        for backend in get_backends():
            if hasattr(backend, "get_all_permissions"):
                permissions.update(backend.get_all_permissions(self, obj))
        return permissions

    def get_short_name(self):
        return self.prenom

    def get_full_name(self):
        return "%s %s" % (self.prenom, self.nom)

    def email_user(self, subject, message, from_email=None, **kwargs):
        """
            Envoie un mail à l'adhérent.
        """
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def email_user_from_template(self, subject, template, language='fr',
                                 from_email=None, context=None, request=None, **kwargs):
        """
            Envoie un mail à l'adhérent en utilisant le template spécifié
        """
        template = loader.get_template("mails/%s/%s" % (template, language))
        context = context or {}
        context.setdefault('adherent', self)
        message = template.render(context=context, request=request)
        self.email_user(subject, message, from_email, **kwargs)

    def get_absolute_url(self):
        """
            Renvoie l'URL associée au profil d'un adhérent
        """
        return reverse('comptes:detail', kwargs={'pk': self.id})

    # Propriétés

    @property
    def last_adhesion(self):
        return self.adhesions.order_by('-fin').first()
