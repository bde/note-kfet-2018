"""
    Formulaires et vues pour le login et la réinitialisation des mots de passe
"""

from django import forms

from django.urls import reverse_lazy

from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes

from django.contrib.auth import get_user_model
from django.contrib.auth.forms import PasswordResetForm
from django.contrib.auth.views import PasswordResetView, PasswordResetConfirmView
from django.contrib.auth.tokens import default_token_generator

from django.contrib.sites.shortcuts import get_current_site

UserModel = get_user_model()

class NotePasswordResetForm(PasswordResetForm):
    """
        Un formulaire de récupération de mot de passe.
        Demande les nom, prénom et adresse mail du compte.
    """
    nom = forms.CharField()
    prenom = forms.CharField(label="Prénom")

    def get_users(self, email, nom, prenom):
        """
            Étant donné un email, un nom et un prénom, renvoie l'ensemble des
            adhérents correspondant à ces critères de recherche.

            Les adhérents inactifs et les superutilisateurs ne sont pas autorisés
            à changer leur mot de passe.
        """
        active_users = UserModel._default_manager.filter(**{
            'nom__iexact' : nom,
            'prenom__iexact' : prenom,
            '%s__iexact' % UserModel.get_email_field_name(): email,
            'is_active': True,
            'is_superuser' : False,
        })
        return (u for u in active_users if u.has_usable_password())

    def save(self, domain_override=None,
             subject_template_name='registration/password_reset_subject.txt',
             email_template_name='registration/password_reset_email.html',
             use_https=False, token_generator=default_token_generator,
             from_email=None, request=None, html_email_template_name=None,
             extra_email_context=None):
        """
            Génère un lien à usage unique pour la réinitialisation du mot de passe
            et l'envoie.
        """
        nom = self.cleaned_data["nom"]
        prenom = self.cleaned_data["prenom"]
        email = self.cleaned_data["email"]
        for user in self.get_users(email, nom, prenom):
            if not domain_override:
                current_site = get_current_site(request)
                site_name = current_site.name
                domain = current_site.domain
            else:
                site_name = domain = domain_override
            context = {
                'email': email,
                'domain': domain,
                'site_name': site_name,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'user': user,
                'token': token_generator.make_token(user),
                'protocol': 'https' if use_https else 'http',
            }
            if extra_email_context is not None:
                context.update(extra_email_context)
            self.send_mail(
                subject_template_name, email_template_name, context, from_email,
                email, html_email_template_name=html_email_template_name,
            )

class NotePasswordResetView(PasswordResetView):
    """
        Vue pour la réinitialisation des mots de passe utilisant le formulaire
        NotePasswordResetForm
    """
    form_class = NotePasswordResetForm
    success_url = reverse_lazy('comptes:password_reset_done')

class NotePasswordResetConfirmView(PasswordResetConfirmView):
    """
        Vue permettant de définir le nouveau mot de passe lors de la réinitialisation.
    """
    success_url = reverse_lazy('comptes:password_reset_complete')
