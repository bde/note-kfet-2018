"""
    Vues de l'application Comptes
"""

from django.conf import settings

from django.urls import reverse_lazy
from django.utils import timezone
from django.shortcuts import redirect, get_object_or_404
from django.core.exceptions import PermissionDenied

from django.views.generic.detail import DetailView
from django.views.generic.edit import FormView, CreateView, UpdateView, DeleteView

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.views import (
    LoginView, LogoutView, PasswordResetDoneView, PasswordResetCompleteView,
)

from django.db.utils import DataError

from django.forms import formset_factory

from django_filters.views import FilterView
from django_filters.rest_framework import DjangoFilterBackend

from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import list_route, detail_route

from note_kfet.utils import to_bool
from note_kfet.droits import Acl, D
from note_kfet.filters import NoteSearchFilter
from note_kfet.views.mixins import NoteMixin
from note_kfet.views.decorators import requires

from comptes.models import (
    Alias, Droit, Accreditation, Adherent, Historique
)
from comptes.forms import (
    AdherentInscriptionForm, AdherentPasswordChangeForm, AdherentDroitForm,
    AdherentSuppressionForm,
)
from comptes.filters import AdherentRechercheFilter
from comptes.serializers import (
    AdherentSerializer, AliasSerializer, HistoriqueSerializer, AccreditationSerializer,
)

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#                                   Vues du site Web
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

### Vues concernant l'authentification

class NoteLoginView(NoteMixin, LoginView):
    """
        Vue d'authentification rajoutant quelques éléments de contexte
    """
    pass

class NoteLogoutView(NoteMixin, LogoutView):
    """
        Vue de déconnexion avec éléments de contexte de la note
    """
    pass

class NotePasswordResetDoneView(NoteMixin, PasswordResetDoneView):
    """
        Vue de réinitialisation de mot de passe avec éléments de contexte de la note
    """
    pass

class NotePasswordResetCompleteView(NoteMixin, PasswordResetCompleteView):
    """
        Vue de réinitialisation de mot de passe avec éléments de contexte de la note
    """
    pass

### Vues concernant le modèle Adhérent

class AdherentInscriptionView(NoteMixin, LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    """
        Vue pour l'inscription d'un nouvel adhérent dans la base de données.
    """
    app_label = "comptes"

    permission_required = D("comptes.adherent_inscrire", Acl.BASIQUE)

    template_name = 'adherent_inscription.html'
    form_class = AdherentInscriptionForm

    def form_valid(self, form):
        user = self.request.user
        if (form.cleaned_data['type'] in [Adherent.CLUB, Adherent.SECTION] and
                not user.has_perm("comptes.adherent_inscrire", Acl.ETENDU)):
            messages.error(
                self.request,
                "Vous ne pouvez pas créer de compte de type %s" % form.cleaned_data['type']
            )
            return self.form_invalid(form)
        elif (form.cleaned_data['type'] in [Adherent.DEBIT] and
              not user.has_perm("comptes.adherent_inscrire", Acl.TOTAL)):
            messages.error(
                self.request,
                "Vous ne pouvez pas créer de compte de type %s" % form.cleaned_data['type']
            )
            return self.form_invalid(form)
        else:
            pass
        return super().form_valid(form)

class AdherentDetailView(NoteMixin, LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    """
        Vue pour l'affichage des données d'un adhérent
    """
    app_label = "comptes"

    permission_required = D("comptes.adherent_detail", Acl.LIMITE)

    template_name = "adherent_detail.html"
    model = Adherent

    def get_object(self, queryset=None):
        obj = super().get_object(queryset=queryset)
        user = self.request.user
        if user != obj and not user.has_perm("comptes.adherent_detail", Acl.BASIQUE):
            messages.error(self.request, "Vous ne pouvez pas consulter le profil de cet adhérent")
            raise PermissionDenied

        return obj

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['liste_droits'] = (
            Droit.objects.filter(content_type__app_label__in=settings.ENABLED_APPS)
            .distinct('codename')
            .order_by('codename', 'niveau')
        )
        return context

class AdherentRechercheView(NoteMixin, LoginRequiredMixin, PermissionRequiredMixin, FilterView):
    """
        Vue pour la recherche avancée dans les comptes
    """
    app_label = "comptes"

    permission_required = D("comptes.adherent_chercher", Acl.BASIQUE)

    template_name = "adherent_recherche.html"
    filterset_class = AdherentRechercheFilter

    def get_filterset_kwargs(self, filterset_class):
        kwargs = super().get_filterset_kwargs(filterset_class)
        kwargs.update({
            'request' : self.request,
        })
        return kwargs

class AdherentUpdateView(NoteMixin, LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    """
        Vue pour la modification d'un adhérent
    """
    app_label = "comptes"

    permission_required = D("comptes.adherent_modifier", Acl.LIMITE)

    template_name = "adherent_modification.html"
    model = Adherent
    fields = [
        'nom', 'prenom', 'sexe', 'email', 'telephone',
        'adresse', 'remunere', 'remarque', 'pbsante', 'is_staff',
    ]

    def get_form(self, form_class=None):
        form = super().get_form(form_class=form_class)
        user = self.request.user
        default_fields = []
        if user == self.object and user.has_perm("comptes.adherent_modifier", Acl.TOTAL):
            fields = Adherent.CHAMPS_MODIFIABLES.get(Acl.TOTAL, default_fields)
        elif user == self.object and user.has_perm("comptes.adherent_modifier", Acl.LIMITE):
            fields = Adherent.CHAMPS_MODIFIABLES.get(Acl.TOTAL, default_fields)
            fields.remove('remarque')
            fields.remove('is_staff')
        elif user.has_perm("comptes.adherent_modifier", Acl.TOTAL):
            fields = Adherent.CHAMPS_MODIFIABLES.get(Acl.TOTAL, default_fields)
        elif user.has_perm("comptes.adherent_modifier", Acl.ETENDU):
            fields = Adherent.CHAMPS_MODIFIABLES.get(Acl.ETENDU, default_fields)
        elif user.has_perm("comptes.adherent_modifier", Acl.BASIQUE):
            fields = Adherent.CHAMPS_MODIFIABLES.get(Acl.BASIQUE, default_fields)
        else:
            fields = default_fields
        for key in form.fields.copy().keys():
            if key not in fields:
                del form.fields[key]
        return form

class AdherentDroitChangeView(NoteMixin, LoginRequiredMixin, FormView):
    """
        Une vue permettant de modifier les droits d'un adhérent.
    """
    app_label = "comptes"

    template_name = "adherent_change_droits.html"
    form_class = formset_factory(AdherentDroitForm, extra=0)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super().get_form_kwargs(*args, **kwargs)
        setattr(self, 'target_user', get_object_or_404(Adherent, pk=self.kwargs.get('pk', None)))
        droits = (
            Droit.objects
            .only('content_type', 'codename')
            .distinct('codename')
            .filter(content_type__app_label__in=settings.ENABLED_APPS)
            .select_related('content_type')
        )
        accreds = Accreditation.objects.filter(
            adherent=self.target_user, droit__codename__in=[d.codename for d in droits],
        )
        kwargs.update({
            'initial' : droits,
            'form_kwargs' : {
                'changed_by' : self.request.user,
                'accreds' : accreds,
            },
        })
        return kwargs

    def form_valid(self, form):
        """
            Une fois que les formulaires sont validés, on crée/modifie/supprime les droits en
            fonction de ce qui a été reçu.
        """
        for subform in form:
            if subform.has_changed() and subform.cleaned_data.get('droit'):
                # Le formulaire a changé, et un droit est défini
                # -> Nouvelle accréditation ou changement de la mention 'méta'
                defaults = {
                    'droit' : subform.cleaned_data['droit'],
                    'meta' : subform.cleaned_data['meta'],
                }
                attr, created = Accreditation.objects.update_or_create(
                    droit__codename=subform.codename, adherent=self.target_user, defaults=defaults,
                )
                attr.save()
            elif subform.has_changed():
                # Le formulaire a changé et l'accréditation a été retirée
                # -> Suppression de l'attribution du droit
                Accreditation.objects.filter(
                    droit__codename=subform.codename, adherent=self.target_user
                ).delete()
            else:
                # Rien n'a changé.
                pass
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({'user_obj' : self.target_user})
        return context

    def get_success_url(self):
        return reverse_lazy("comptes:detail", kwargs={'pk' : self.target_user.id})

class AdherentPasswordChangeView(NoteMixin, LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    """
        Une vue pour changer le mot de passe d'un utilisateur.
    """
    app_label = "comptes"

    permission_required = D("comptes.adherent_change_pw", Acl.LIMITE)

    template_name = "adherent_change_password.html"
    form_class = AdherentPasswordChangeForm
    model = Adherent

    def form_valid(self, form):
        user = self.request.user
        if (self.object == user
            or user.has_perm("comptes.adherent_change_pw", Acl.TOTAL)
            or (self.object.type == Adherent.PERSONNE
                and user.has_perm("comptes.adherent_change_pw", Acl.BASIQUE))
            or (self.object.type in [Adherent.CLUB, Adherent.SECTION]
                and user.has_perm("comptes.adherent_change_pw", Acl.ETENDU))):
            messages.success(self.request, "Mot de passe changé avec succès")
        else:
            messages.error(
                self.request, "Vous ne pouvez pas changer le mot de passe de cet adhérent"
            )

        return super().form_valid(form)

class AdherentAvatarView(NoteMixin, LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    """
        Vue pour la modification de l'avatar d'un adhérent.
    """
    app_label = "comptes"

    permission_required = D("comptes.adherent_modifier", Acl.LIMITE)

    template_name = "adherent_avatar.html"
    model = Adherent
    fields = ['avatar']

    def get_success_url(self):
        adherent = get_object_or_404(Adherent, pk=self.kwargs['pk'])
        return reverse_lazy('comptes:detail', kwargs={'pk' : adherent.id})

class AdherentDeleteView(NoteMixin, LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    """
        Vue pour la suppression d'un adhérent.

        Une page de confirmation est affichée et permet de choisir
        d'effacer les données d'un compte.
    """
    app_label = "comptes"

    permission_required = D("comptes.adherent_supprimer", Acl.BASIQUE)

    template_name = "adherent_suppression.html"
    model = Adherent

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = AdherentSuppressionForm()

        return context

    def delete(self, request, *args, **kwargs):
        form = AdherentSuppressionForm(request.POST)
        user = self.request.user
        if form.is_valid():
            obj = self.get_object()
            shred = form.cleaned_data['shred']
            if (obj.type != Adherent.PERSONNE and
                    not user.has_perm("comptes.adherent_supprimer", Acl.ETENDU)):
                messages.error(self.request, "Vous ne pouvez pas supprimer ce type d'adhérent")
                return redirect("comptes:detail", pk=obj.id)
            elif shred and not user.has_perm("comptes.adherent_supprimer", Acl.TOTAL):
                messages.warning(
                    self.request,
                    "Le compte a été supprimé, mais il n'a pas été anonymisé"
                )
            elif shred:
                obj.shred()
                obj.save()
            else:
                pass

        return super().delete(request, *args, **kwargs)

    def get_success_url(self, *args, **kwargs):
        return reverse_lazy('comptes:detail', kwargs={'pk' : self.object.id})

class AliasesHistoriqueView(NoteMixin, LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    """
        Vue permettant de consulter l'historique d'attribution d'un pseudo
    """
    app_label = "comptes"

    permission_required = D("comptes.adherent_detail", Acl.BASIQUE)

    template_name = "alias_historique.html"
    model = Alias

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'historique' : Historique.objects.filter(alias=self.get_object()).order_by('-date'),
        })
        return context

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#                                       Vues de l'API
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class AdherentViewSet(viewsets.GenericViewSet):
    """
        Ensemble de vues pour travailler avec des objets Adhérent via l'API
    """
    queryset = Adherent.objects.all()
    serializer_class = AdherentSerializer
    search_fields = ('pseudo', 'aliases__alias',)
    filter_fields = {
        'id' : ['exact'],
        'pseudo' : ['exact', 'iregex', 'istartswith', 'icontains'],
        'aliases__alias' : ['exact', 'iregex', 'istartswith', 'icontains'],
        'aliases_acquis__alias__alias' : ['exact', 'iregex', 'istartswith', 'icontains'],
        'nom' : ['exact', 'iregex', 'istartswith', 'icontains'],
        'prenom' : ['exact', 'iregex', 'istartswith', 'icontains'],
        'email' : ['exact', 'iregex', 'istartswith', 'icontains'],
        'type' : ['in'],
        'telephone' : ['exact', 'iregex', 'istartswith'],
        'remunere' : ['exact'],
        'supprime' : ['exact'],
        'is_active' : ['exact'],
    }

    @requires("comptes.adherent_chercher", Acl.BASIQUE)
    def list(self, request):
        """
            Effectue une recherche parmi les adhérents.
            Les données doivent :
            -   pour une recherche rapide :
                *   être envoyées via une requête GET,
                *   contenir un paramètre 'search' contenant le terme à rechercher,
                *   contenir un paramètre 'regex' pouvant valoir 'true' ou 'false' indiquant si
                    l'expression de recherche est une expression régulière
            -   pour une recherche avancée :
                *   être envoyées via une requête POST,
                *   contenir autant de paramètres que nécéssaire pour la recherche.
                    La liste des paramètres utilisables est disponible avec une requête OPTIONS.
        """
        if not request.query_params.keys():
            return Response(
                {"detail" : "Aucun paramètre de recherche fourni"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        elif "search" in request.query_params:
            # Recherche rapide
            self.filter_backends = (NoteSearchFilter,)
        elif request.user.has_perm("comptes.adherent_chercher", Acl.ETENDU):
            # Recherche avancée
            self.filter_backends = (DjangoFilterBackend,)
        else:
            return Response(
                {"detail" : "Vous ne pouvez pas effectuer de recherche avancée"},
                status=status.HTTP_403_FORBIDDEN,
            )

        advanced_fields = {'is_active', 'supprime', 'type__in', 'adhesions__fin__gte'}
        if (advanced_fields & set(request.query_params.keys())
                and not request.user.has_perm("comptes.adherent_chercher", Acl.TOTAL)):
            return Response(
                {
                    "detail" : "Vous ne pouvez pas filtrer les comptes actifs, les comptes "
                               "supprimés, ou par type de compte.",
                },
                status=status.HTTP_403_FORBIDDEN,
            )

        query_params = request.query_params.copy()
        query_params.setdefault('is_active', 'true')
        query_params.setdefault('supprime', 'false')
        query_params.setdefault('adhesions__fin__gte', timezone.now().isoformat())
        query_params.setdefault('type__in', Adherent.PERSONNE)

        #FIXME: Accès à un attribut protégé, il faudrait faire mieux
        self.request._request.GET = query_params
        qs = self.filter_queryset(self.get_queryset()).order_by('id').distinct('id')
        fields = ['id', 'pseudo', 'nom', 'prenom', 'type', 'aliases', 'last_adhesion']
        try:
            return Response(self.get_serializer(qs, many=True, fields=fields).data)
        except DataError:
            return Response(
                {'detail' : 'La RegExp donnée doit être une RegExp PostgreSQL valide'},
                status=status.HTTP_400_BAD_REQUEST,
            )

    @requires("comptes.adherent_inscrire", Acl.BASIQUE)
    def create(self, request):
        """
            Crée un nouvel adhérent dans la base de données et renvoie son identifiant
            Les données doivent :
            -   être envoyées via un requête POST
            -   ne contenir que des champs indiqués en réponse à une requête OPTIONS
        """
        fields = [
            'pseudo', 'nom', 'prenom', 'email', 'password', 'sexe', 'type',
            'telephone', 'adresse', 'remunere', 'pbsante', 'remarque', 'section',
        ]

        serializer = self.get_serializer(data=request.data, fields=fields)
        serializer.is_valid(raise_exception=True)

        authorized_types = [Adherent.PERSONNE]
        if request.user.has_perm("comptes.adherent_inscrire", Acl.ETENDU):
            authorized_types += [Adherent.CLUB, Adherent.SECTION]
        if request.user.has_perm("comptes.adherent_inscrire", Acl.TOTAL):
            authorized_types += [Adherent.DEBIT]

        if serializer.validated_data.get('type') in authorized_types:
            return Response(
                {"detail" : "Vous ne pouvez pas inscrire ce type d'adhérent"},
                status=status.HTTP_403_FORBIDDEN,
            )

        new_adh = serializer.save()
        return Response({'id' : new_adh.id}, status=status.HTTP_201_CREATED)

    @requires("comptes.adherent_detail")
    def retrieve(self, request, pk=None):
        """
            Renvoie les informations d'un adhérent
            Les données doivent :
            -   être envoyées via un requête GET
        """
        fields = []
        target_user = self.get_object()

        if request.user.has_perm("comptes.adherent_detail", Acl.TOTAL):
            # Accès à toutes les données sans limitation
            fields = Adherent.CHAMPS_VISIBLES[Acl.TOTAL]
        elif (request.user.has_perm("comptes.adherent_detail", Acl.LIMITE)
              and request.user == target_user):
            # Accès à toutes les données, sauf aux remarques
            fields = list(set(Adherent.CHAMPS_VISIBLES[Acl.TOTAL]) - {'remarque'})
        elif request.user.has_perm("comptes.adherent_detail", Acl.ETENDU):
            # Accès aux informations personnelles de l'adhérent
            fields = Adherent.CHAMPS_VISIBLES[Acl.ETENDU]
        elif request.user.has_perm("comptes.adherent_detail", Acl.BASIQUE):
            # Accès aux informations basiques sur l'adhérent
            fields = Adherent.CHAMPS_VISIBLES[Acl.BASIQUE]
        else:
            return Response(
                {'detail': "Vous ne pouvez pas consulter le profil de cet adhérent"},
                status=status.HTTP_403_FORBIDDEN,
            )

        serializer = self.get_serializer(target_user, fields=fields)
        return Response(serializer.data)

    @requires("comptes.adherent_modifier")
    def partial_update(self, request, pk=None):
        """
            Met à jour les données d'un adhérent.
            Les données doivent :
            -   être envoyées via une requête PATCH
        """
        fields = []
        target_user = self.get_object()

        if request.user.has_perm("comptes.adherent_modifier", Acl.TOTAL):
            # Toutes les données peuvent être modifiées
            fields = Adherent.CHAMPS_MODIFIABLES[Acl.TOTAL]
        elif (request.user.has_perm("comptes.adherent_modifier", Acl.LIMITE)
              and request.user.id == target_user.id):
            # Toutes les données sont modifiables, sauf les remarques et
            # l'indication du statut de membre actif
            fields = list(set(Adherent.CHAMPS_MODIFIABLES[Acl.TOTAL]) - {'remarque', 'is_staff'})
        elif request.user.has_perm("comptes.adherent_modifier", Acl.ETENDU):
            # Les informations personnelles de l'adhérent sont modifiables
            fields = Adherent.CHAMPS_MODIFIABLES[Acl.ETENDU]
        elif request.user.has_perm("comptes.adherent_modifier", Acl.BASIQUE):
            # Seules les données basiques sont modifiables
            fields = Adherent.CHAMPS_MODIFIABLES[Acl.BASIQUE]
        else:
            return Response(
                {'detail': 'Vous ne pouvez pas effectuer cette modification sur cet adhérent'},
                status=status.HTTP_403_FORBIDDEN,
            )

        serializer = self.get_serializer(target_user, data=request.data, partial=True, fields=fields)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({}, status=status.HTTP_200_OK)

    @requires("comptes.adherent_supprimer", Acl.BASIQUE)
    def destroy(self, request, pk=None):
        """
            Supprime un adhérent.
            Si l'anonymisation a été demandée, renvoie un booléen 'anonymisé' indiquant
            si le compte a été anonymisé, avec un message d'explication si la demande a été ignorée.
            Les données doivent :
            -   être envoyées via un requête DELETE
            -   comporter un paramètre booléen 'anonymiser' indiquant s'il faut anonymiser
                le compte. Ce paramètre vaut 'false' par défaut.
        """
        target_user = self.get_object()
        if target_user == request.user:
            return Response(
                {'detail' : "Vous ne pouvez pas supprimer votre propre compte"},
                status=status.HTTP_403_FORBIDDEN,
            )

        cleanup = to_bool(request.data.get('anonymiser', 'false'))
        if cleanup is None:
            return Response(
                {'detail' : "Paramètre 'anonymiser' non valide"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        if (target_user.type != Adherent.PERSONNE
                and not request.user.has_perm("comptes.adherent_supprimer", Acl.ETENDU)):
            return Response(
                {"detail" : "Vous n'avez pas le droit de supprimer ce type de compte"},
                status=status.HTTP_403_FORBIDDEN,
            )

        if not cleanup:
            shred = False
            detail = ""
        elif not request.user.has_perm("comptes.adherent_supprimer", Acl.TOTAL):
            shred = False
            detail = "Vous ne pouvez pas anonymiser un compte"
        else:
            shred = True
            detail = ""

        target_user.delete(shred=shred)
        return Response({"anonymisé": shred, "detail" : detail}, status=status.HTTP_204_NO_CONTENT)

    @detail_route(methods=['patch'])
    @requires("comptes.adherent_change_pw")
    def change_password(self, request, pk=None):
        """
            Effectue le changement de mot de passe d'un adhérent.
            Les données doivent :
            -   être envoyées via une requête PATCH
            -   contenir le nouveau mot de passe dans le paramètre 'password'
            -   Si l'adhérent n'est pas suffisamment accrédité, il doit également
                fournir l'ancien mot de passe dans le champ 'old_password'
        """
        target_user = self.get_object()
        if (request.user.has_perm("comptes.adherent_change_pw", Acl.LIMITE)
                and request.user == target_user):
            # Changement de son propre mot de passe en renseignant le précédent
            need_old_pass = True
        elif target_user.is_superuser:
            # Personne ne peut changer le mot de passe d'un superutilisateur, mis
            # à part lui même
            return Response(
                {"detail" : "Vous ne pouvez pas changer ce mot de passe"},
                status=status.HTTP_403_FORBIDDEN,
            )
        elif request.user.has_perm("comptes.adherent_change_pw", Acl.TOTAL):
            # Changement de n'importe quel mot de passe sans renseigner le
            # précédent
            need_old_pass = False
        elif (target_user.type == Adherent.PERSONNE
              and request.user.has_perm("comptes.adherent_change_pw", Acl.ETENDU)):
            # Changement du mot de passe d'une autre personne
            need_old_pass = True
        else:
            # Pas suffisamment de droits pour changer un mot de passe
            return Response(
                {"detail" : "Vous ne pouvez pas changer ce mot de passe"},
                status=status.HTTP_403_FORBIDDEN,
            )

        # On vérifie l'ancien mot de passe
        old_pass = request.data.get('old_password', None)
        if need_old_pass and not target_user.check_password(old_pass):
            return Response(
                {"detail" : "Ancien mot de passe incorrect"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        serializer = self.get_serializer(target_user, data=request.data, fields=['password'])
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({}, status=status.HTTP_200_OK)

    @detail_route(methods=['patch'])
    @requires("comptes.adherent_desactiver", Acl.BASIQUE)
    def active(self, request, pk=None):
        """
            Active ou désactive un compte.
            Les données doivent :
            -   être envoyées via une requête PATCH
            -   contenir un paramètre 'active' pouvant valoir True ou False
        """
        target_user = self.get_object()
        if target_user.id == request.user.id:
            return Response(
                {"detail" : "Vous ne pouvez pas activer/désactiver votre propre compte"},
                status=status.HTTP_403_FORBIDDEN,
            )

        user_types = [Adherent.PERSONNE]
        if request.user.has_perm("comptes.adherent_desactiver", Acl.ETENDU):
            user_types += [Adherent.CLUB, Adherent.SECTION]
        if request.user.has_perm("comptes.adherent_desactiver", Acl.TOTAL):
            user_types += [Adherent.DEBIT]

        if target_user.type not in user_types:
            return Response(
                {"detail" : "Vous ne pouvez pas activer/désactiver ce type de compte"},
                status=status.HTTP_403_FORBIDDEN,
            )

        serializer = self.get_serializer(target_user, data=request.data, fields=['is_active'])
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response({}, status=status.HTTP_200_OK)

class AliasViewSet(viewsets.GenericViewSet):
    """
        Ensemble de vues permettant de gérer les aliases
    """
    queryset = Alias.objects.all()
    serializer_class = AliasSerializer

    @requires("comptes.adherent_detail", Acl.BASIQUE)
    def retrieve(self, request, pk=None):
        """
            Consulte les informations liées à un alias donné
        """
        serialized_alias = self.get_serializer(self.get_object())
        qs = Historique.objects.filter(alias=pk).order_by('-date')
        serialized_history = HistoriqueSerializer(qs, many=True)
        return Response(
            {
                "alias" : serialized_alias.data,
                "history" : serialized_history.data,
            }
        )

    @requires("comptes.alias_adopter")
    def create(self, request):
        """
            Crée un nouvel alias.
            Renvoie l'identifiant du nouvel alias en cas de succès.
            Les données doivent être envoyées via une requête POST
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        if not (request.user.has_perm("comptes.alias_adopter", Acl.TOTAL)
                or request.user == serializer.validated_data['proprietaire']):
            return Response(
                {"detail" : "Vous ne pouvez pas adopter cet alias"},
                status=status.HTTP_403_FORBIDDEN,
            )

        new_alias = serializer.save()
        return Response({'id' : new_alias.id}, status=status.HTTP_201_CREATED)

    @requires("comptes.alias_abandonner")
    def destroy(self, request, pk=None):
        """
            Retire un alias.
            Les données doivent être envoyées via une requête DELETE
        """
        alias = self.get_object()
        if not (request.user.has_perm("comptes.alias_abandonner", Acl.TOTAL)
                or request.user == alias.proprietaire):
            return Response(
                {"detail" : "Vous ne pouvez pas abandonner cet alias"},
                status=status.HTTP_403_FORBIDDEN,
            )

        alias.delete()
        return Response({}, status=status.HTTP_204_NO_CONTENT)

    @requires("comptes.alias_adopter")
    def partial_update(self, request, pk=None):
        """
            Attribue un alias existant.
            L'alias ne doit pas être le pseudo de l'adhérent.
            Les données envoyées doivent:
            -   être envoyées via une requête PATCH
        """
        serializer = self.get_serializer(
            self.get_object(), data=request.data, fields=['proprietaire'],
        )
        serializer.is_valid(raise_exception=True)
        if not (request.user.has_perm("comptes.alias_adopter", Acl.TOTAL)
                or request.user == serializer.validated_data['proprietaire']):
            return Response(
                {"detail" : "Vous ne pouvez pas adopter cet alias"},
                status=status.HTTP_403_FORBIDDEN,
            )

        serializer.save()
        return Response({}, status=status.HTTP_200_OK)

    @list_route(methods=['get'])
    def available(self, request):
        """
            Interroge la base de données sur la disponibilité d'un alias donné.
            Renvoie l'identifiant de l'alias s'il existe, null sinon.
            Les données doivent :
            -   être envoyées via une requête GET
            -   contenir un paramètre 'alias' contenant l'alias concerné.
        """
        alias = request.query_params.get('alias')
        if alias is None:
            return Response(
                {'detail' : "Alias non renseigné ou incorrect"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        try:
            normalized_alias = Adherent.normalize_username(alias)
            alias = Alias.objects.get(alias=normalized_alias)
            status_code = (
                status.HTTP_200_OK if alias.proprietaire is None else status.HTTP_403_FORBIDDEN
            )
            return Response({'id' : alias.id}, status=status_code)
        except Alias.DoesNotExist:
            return Response({'id' : None}, status=status.HTTP_200_OK)

class AccreditationViewSet(viewsets.GenericViewSet):
    """
        Un ensemble de vues pour gérer les attributions de droits
    """
    queryset = Accreditation.objects.all()
    serializer_class = AccreditationSerializer

    def create(self, request):
        """
            Permet de donner une nouvelle accréditation.
            Une accréditation pour le même nom de code ne doit pas déjà exister.
            Les données doivent :
            -   être envoyées via une requête POST
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        target_user = serializer.validated_data['adherent']
        droit = serializer.validated_data['droit']
        if (
                self.get_queryset()
                .filter(adherent=target_user, droit__codename=droit.codename)
                .exists()
            ):
            return Response(
                {'detail' : "Cette attribution existe déjà."},
                status=status.HTTP_304_NOT_MODIFIED,
            )

        if not request.user.has_perm(
                "%s.%s" % (droit.content_type.app_label, droit.codename),
                droit.niveau,
                meta=True,
            ):
            # Le requérant n'a pas le méta-droit nécéssaire pour continuer
            return Response(
                {"detail" : "Vous n'avez pas le méta-droit nécéssaire pour attribuer ce droit"},
                status=status.HTTP_403_FORBIDDEN,
            )

        meta = serializer.validated_data['meta']
        if meta and not request.user.is_superuser:
            # Un méta-droit ne peut être donné que par un superutilisateur
            return Response(
                {"detail" : "Vous ne pouvez pas attribuer de méta-droit"},
                status=status.HTTP_403_FORBIDDEN,
            )
        else:
            new_accred = serializer.save()
            return Response({'id' : new_accred.id}, status=status.HTTP_201_CREATED)

    def destroy(self, request, pk=None):
        """
            Permet de révoquer l'attribution d'un droit à un utilisateur.
            Les données doivent :
            -   être envoyées via une requête DELETE
        """
        accred = self.get_object()
        codename = "%s.%s" % (accred.content_type.app_label, accred.droit.codename)
        if accred.meta and not request.user.is_superuser:
            # Un utilisateur normal n'est pas habilité à révoquer une accréditation
            # avec un mention 'méta'
            return Response(
                {"detail" : "Vous ne pouvez pas révoquer une accréditation 'méta'"},
                status=status.HTTP_403_FORBIDDEN,
            )
        if not request.user.has_perm(codename, accred.niveau, meta=True):
            # L'utilisateur n'est pas habilité à supprimer ce droit
            return Response(
                {"detail" : "Vous n'êtes pas habilité à révoquer cette accréditation"},
                status=status.HTTP_403_FORBIDDEN,
            )
        accred.delete()
        return Response({}, status=status.HTTP_204_NO_CONTENT)
