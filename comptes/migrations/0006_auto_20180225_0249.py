# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-02-25 01:49
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('comptes', '0005_auto_20180207_2231'),
    ]

    operations = [
        migrations.AlterField(
            model_name='adherent',
            name='pseudo',
            field=models.CharField(max_length=1023, unique=True, validators=[django.core.validators.RegexValidator('^#[0-9]+$', inverse_match=True), django.core.validators.RegexValidator('^(\\S|\\S+.*\\S+)$')]),
        ),
        migrations.AlterField(
            model_name='alias',
            name='alias',
            field=models.TextField(unique=True, validators=[django.core.validators.RegexValidator('^__deleted__[0-9]+$', inverse_match=True), django.core.validators.RegexValidator('^#[0-9]+$', inverse_match=True), django.core.validators.RegexValidator('^(\\S|\\S+.*\\S+)$')], verbose_name='alias'),
        ),
    ]
