"""
    Validateurs utilisés par les modèles de l'application Comptes
"""

from django.core.validators import RegexValidator

## Regex utilisées pour la vérification des champs de modèles

PSEUDO_REGEX = r"^(\S|\S+.*\S+)$"       # Pas de leading/trailing spaces
# XXX : Limiter aussi le nombre maximal d'espaces consécutifs dans le pseudo ?
#       |---> r"(\S|\S+(\S|\s{,n})*\S+)", avec n bien choisi ?
DELETED_REGEX = r"^__deleted__[0-9]+$"  # Les pseudos en '__deleted__[0-9]+' sont réservés
ID_REGEX = r'^#[0-9]+$'                 # Les pseudos en '#<nombre>' sont réservés à l'identification via ID BdE
TELEPHONE_REGEX = r"^[0-9 -]+$"

class PseudoValidator(RegexValidator):
    regex = PSEUDO_REGEX

class DeletedValidator(RegexValidator):
    regex = DELETED_REGEX

class IDValidator(RegexValidator):
    regex = ID_REGEX

class TelephoneValidator(RegexValidator):
    regex = TELEPHONE_REGEX
