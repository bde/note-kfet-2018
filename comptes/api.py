"""
    Vues de l'API de l'application « Comptes »
"""

from rest_framework import routers

from comptes.views import AdherentViewSet, AliasViewSet

router = routers.DefaultRouter()

router.register(r'aliases', AliasViewSet)
router.register(r'adherents', AdherentViewSet)

urlpatterns = router.urls
