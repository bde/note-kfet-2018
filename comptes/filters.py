"""
    Filtres de l'application Comptes
"""

from django.utils import timezone
from django.db.models import Q

import django_filters as filters

from note_kfet.droits import Acl

from comptes.models import Adherent, Section

class AdherentRechercheFilter(filters.FilterSet):
    """
        Filtre pour rechercher un adhérent en utilisant un terme
        qui sera cherché dans le pseudo, le nom, le prénom.
    """
    nom = filters.CharFilter(
        label="Nom", lookup_expr="icontains",
    )
    prenom = filters.CharFilter(
        label="Prénom", lookup_expr="icontains",
    )
    aliases__alias = filters.CharFilter(
        label="Pseudo/Alias", lookup_expr="icontains",
    )
    adhesions__section__sigle = filters.ModelChoiceFilter(
        label="Section", queryset=Section.objects.all(),
    )

    class Meta:
        model = Adherent
        fields = [
            'nom', 'prenom', 'aliases__alias', 'sexe', 'adhesions__section__sigle',
            'type', 'email', 'telephone', 'adresse', 'remunere',
        ]

    @property
    def qs(self):
        """
            Filtre le queryset en fonction de l'accréditation de l'utilisateur
            utilisant le filtre, en accord avec l'ACL_MAP de la recherche parmi
            les comptes.
        """
        # TODO: Permettre à l'utilisateur de choisir la clause order_by
        qs = super().qs.order_by('id')
        try:
            if not self.request.GET:
                # Premier affichage de la page de recherche -> Aucun résultat
                qs = qs.none()
            if not self.request.user.has_perm("comptes.adherent_chercher", Acl.TOTAL):
                # Pas d'adhérent supprimé
                qs = qs.filter(supprime=False)
            if not self.request.user.has_perm("comptes.adherent_chercher", Acl.ETENDU):
                # Pas d'ancien adhérent
                # Il faut tout de même inclure les clubs/sections/débit qui n'adhèrent pas
                qs = qs.filter(
                    ~Q(type__in=[Adherent.PERSONNE]) | Q(adhesions__fin__gt=timezone.now())
                )
            return qs
        except AttributeError:
            # La requête n'a pas été fournie -> Aucun résultat
            return qs.none()
