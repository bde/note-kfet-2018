from django.test import TestCase
from django.db import IntegrityError
from django.core.exceptions import ValidationError

from comptes.models import Adherent

class AdherentTests(TestCase):
    """
        Tests sur la classe Adhérent
    """
    def setUp(self):
        Adherent.objects.create(pseudo="Adhérent 1", password="plopisnotsecured", nom="adhérent", prenom="un", email="adh1@example.com", type="personne", sexe="M")
        Adherent.objects.create(pseudo="Adhérent 2", password="plopisnotsecured", nom="adhérent", prenom="deux", email="adh2@example.com", type="personne", is_superuser=True, sexe="F")
        Adherent.objects.create(pseudo="Club 1", password="plopisnotsecured", nom="club", prenom="un", email="clu1@example.com", type="club")
        Adherent.objects.create(pseudo="Section 1", password="plopisnotsecured", nom="section", prenom="un", email="sec1@example.com", type="section")
        Adherent.objects.create(pseudo="Débit 1", password="plopisnotsecured", nom="débit", prenom="un", email="deb1@example.com", type="débit")

    def test_adherent_non_personne_ne_peut_avoir_sexe(self):
        clu1 = Adherent.objects.get(pseudo="Club 1")
        sec1 = Adherent.objects.get(pseudo="Section 1")
        deb1 = Adherent.objects.get(pseudo="Débit 1")

        for obj in [clu1, sec1, deb1]:
            with self.assertRaises(ValidationError):
                obj.sexe = "M"
                obj.save()

    def test_seul_personne_peut_devenir_superuser(self):
        adh1 = Adherent.objects.get(pseudo="Adhérent 1")
        adh2 = Adherent.objects.get(pseudo="Adhérent 2")
        clu1 = Adherent.objects.get(pseudo="Club 1")
        sec1 = Adherent.objects.get(pseudo="Section 1")
        deb1 = Adherent.objects.get(pseudo="Débit 1")

        adh1.is_superuser = True
        adh1.save()
        self.assertTrue(adh1.is_superuser)

        adh2.is_superuser = False
        adh2.save()
        self.assertFalse(adh2.is_superuser)

        with self.assertRaises(ValidationError):
            clu1.is_superuser = True
            clu1.save()

        with self.assertRaises(ValidationError):
            sec1.is_superuser = True
            sec1.save()

        with self.assertRaises(ValidationError):
            deb1.is_superuser = True
            deb1.save()

    def test_seul_personne_peut_devenir_staff(self):
        adh1 = Adherent.objects.get(pseudo="Adhérent 1")
        adh2 = Adherent.objects.get(pseudo="Adhérent 2")
        clu1 = Adherent.objects.get(pseudo="Club 1")
        sec1 = Adherent.objects.get(pseudo="Section 1")
        deb1 = Adherent.objects.get(pseudo="Débit 1")

        adh1.is_staff = True
        adh1.save()
        self.assertTrue(adh1.is_staff)

        adh2.is_staff = False
        adh2.save()
        self.assertFalse(adh2.is_staff)

        for obj in [clu1, sec1, deb1]:
            with self.assertRaises(ValidationError):
                obj.is_staff = True
                obj.save()

    def test_suppression_adherent_echoue(self):
        adh1 = Adherent.objects.get(pseudo="Adhérent 1")
        adh2 = Adherent.objects.get(pseudo="Adhérent 2")
        clu1 = Adherent.objects.get(pseudo="Club 1")
        sec1 = Adherent.objects.get(pseudo="Section 1")
        deb1 = Adherent.objects.get(pseudo="Débit 1")

        for obj in [adh1, adh2, clu1, sec1, deb1]:
            obj.delete()
            obj.refresh_from_db()
            self.assertTrue(obj.supprime)

    def test_nom_doit_commencer_et_finir_par_lettre(self):
        adh1 = Adherent.objects.get(pseudo="Adhérent 1")
        adh2 = Adherent.objects.get(pseudo="Adhérent 2")
        clu1 = Adherent.objects.get(pseudo="Club 1")
        sec1 = Adherent.objects.get(pseudo="Section 1")
        deb1 = Adherent.objects.get(pseudo="Débit 1")

        for obj in [adh1, adh2, clu1, sec1, deb1]:
            obj.pseudo = "\tpseudo"
            with self.assertRaises(ValidationError):
                adh1.save()

            obj.pseudo = "pseudo "
            with self.assertRaises(ValidationError):
                adh1.save()

            obj.pseudo = " pseudo x\t"
            with self.assertRaises(ValidationError):
                adh1.save()

    def test_shred_supprime_donnees_sensibles(self):
        adh1 = Adherent.objects.get(pseudo="Adhérent 1")
        adh2 = Adherent.objects.get(pseudo="Adhérent 2")
        clu1 = Adherent.objects.get(pseudo="Club 1")
        sec1 = Adherent.objects.get(pseudo="Section 1")
        deb1 = Adherent.objects.get(pseudo="Débit 1")

        for obj in [adh1, adh2, clu1, sec1, deb1]:
            obj.shred()
            self.assertEqual(obj.pseudo, "__deleted__%d" % obj.id)
            self.assertEqual(obj.nom, "__deleted__%d" % obj.id)
            self.assertEqual(obj.prenom, "__deleted__%d" % obj.id)
            self.assertIsNone(obj.sexe)
            self.assertEqual(obj.email, "nobody@crans.org")
            self.assertEqual(obj.adresse, "")
            self.assertEqual(obj.telephone, "")
            self.assertEqual(obj.remarque, "")
            self.assertEqual(obj.pbsante, "")
            self.assertFalse(obj.is_superuser)
            self.assertFalse(obj.is_staff)
            self.assertFalse(obj.is_active)
            self.assertFalse(obj.remunere)
