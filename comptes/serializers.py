"""
    Sérialiseurs de l'app « Comptes »
"""

from django.contrib.auth.password_validation import validate_password as django_validate_password

from rest_framework import serializers

from note_kfet.serializers import mixins
from note_kfet.serializers import Base64ImageField

from comptes.models import Section, Alias, Historique, Droit, Accreditation, Adhesion, Adherent

class SectionSerializer(serializers.ModelSerializer):
    """
        Sérialiseur pour le modèle Section
    """
    class Meta:
        model = Section
        fields = ['sigle', 'nom']
        read_only_fields = ['sigle', 'nom']

class DroitSerializer(serializers.ModelSerializer):
    """
        Sérialiseur pour le modèle Droit
    """
    class Meta:
        model = Droit
        fields = ['id', 'description', 'niveau']
        read_only_fields = ['id', 'description', 'niveau']

class AccreditationSerializer(mixins.DynamicFieldsMixin, serializers.ModelSerializer):
    """
        Sérialiseur pour le modèle Accréditation
    """
    droit = DroitSerializer(read_only=True)
    class Meta:
        model = Accreditation
        fields = ['id', 'adherent', 'droit', 'meta']
        read_only_fields = ['id', 'adherent', 'droit']

class AdhesionSerializer(serializers.ModelSerializer):
    """
        Sérialiseur pour le modèle Adhesion
    """
    section = SectionSerializer(read_only=True)
    class Meta:
        model = Adhesion
        fields = ['debut', 'fin', 'section']
        read_only_fields = ['debut', 'fin']

### Sérialiseurs pour le modèle Alias

class AliasSerializer(mixins.DynamicFieldsMixin, serializers.ModelSerializer):
    """
        Sérialiseur pour le modèle Alias
    """
    class Meta:
        model = Alias
        fields = ['alias', 'proprietaire']
        default_empty = False

    def validate_alias(self, value):
        """
            Normalise l'alias avant de vérifier s'il n'est pas déjà
            présent et de le créer
        """
        normalized_alias = Adherent.normalize_username(value)
        if self.Meta.model.objects.filter(alias=normalized_alias).exists():
            raise serializers.ValidationError("Cet alias est déjà pris")
        return normalized_alias

    def update(self, instance, validated_data):
        """
           Effectue le changement de propriétaire d'un alias
        """
        if instance.is_pseudo:
            raise serializers.ValidationError(
                "Cet alias ne peut être abandonné, il s'agit d'un pseudo"
            )
        return super().update(instance, validated_data)

### Sérialiseurs pour le modèle Historique

class HistoriqueSerializer(serializers.ModelSerializer):
    """
        Sérialiseur pour le modèle Historique
    """
    class Meta:
        model = Historique
        fields = ['date', 'precedent', 'suivant']

### Sérialiseurs pour le modèle Adhérent

class AdherentSerializer(mixins.DynamicFieldsMixin, serializers.ModelSerializer):
    """
        Sérialiseur pour le modèle Adhérent
    """
    accreditations = AccreditationSerializer(many=True, read_only=True, fields=['droit', 'meta'])
    aliases = serializers.SlugRelatedField(many=True, read_only=True, slug_field='alias')
    adhesions = AdhesionSerializer(many=True, read_only=True)
    last_adhesion = AdhesionSerializer(read_only=True)
    section = serializers.CharField(max_length=10)
    class Meta:
        model = Adherent
        fields = [
            'id', 'pseudo', 'nom', 'prenom', 'email', 'sexe',
            'type', 'telephone', 'adresse', 'remunere', 'pbsante', 'remarque',
            'supprime', 'is_staff', 'is_active', 'aliases', 'adhesions', 'last_adhesion',
            'accreditations', 'password', 'section', 'avatar',
        ]
        read_only_fields = ['id']
        extra_kwargs = {'password': {'write_only': True}}

    def validate_section(self, value):
        """
            Vérifie que la section demandée n'est pas fermée
        """
        qs = Section.objects.filter(sigle=value)
        if not qs.exists():
            raise serializers.ValidationError("Cette section n'existe pas")
        elif qs.get().ferme:
            raise serializers.ValidationError("Cette section est fermée.")
        else:
            return value

    def validate_pseudo(self, value):
        """
            Vérifie que le pseudo demandé n'est pas déjà pris
        """
        pseudo = Adherent.normalize_username(value)
        qs = Alias.objects.filter(alias=pseudo)
        if not self.instance and qs.filter(proprietaire__isnull=False).exists():
            raise serializers.ValidationError("Le pseudo %s est déjà pris." % pseudo)
        elif self.instance and not qs.filter(proprietaire=self.instance).exists():
            raise serializers.ValidationError("%s n'est pas l'un de vos aliases" % pseudo)
        else:
            return pseudo

    def validate_password(self, value):
        """
            Vérifie que le mot de passe fourni répond aux exigences imposées
        """
        django_validate_password(value)
        return value

    def validate(self, data):
        """
            Vérifie que le pseudo demandé n'est pas déjà pris, que
            la section du nouvel adhérent n'est pas fermée et que le
            formulaire est complet relativement au type de compte.
        """
        obj_type = data.get('type', self.instance.type)
        obj_nom = data.get('nom', self.instance.nom)
        obj_prenom = data.get('prenom', self.instance.prenom)
        if obj_type != Adherent.DEBIT and not obj_nom:
            self.add_error(
                'nom',
                serializers.ValidationError("Veuillez rensigner votre nom."),
            )
        if obj_type == Adherent.PERSONNE and not obj_prenom:
            self.add_error(
                'prenom',
                serializers.ValidationError("Veuillez rensigner votre prénom."),
            )

        return data

    def update(self, instance, validated_data):
        """
            Mets à jour un adhérent.
            Cette méthode est surchargée pour gérer le cas d'un changement de mot de passe.
        """
        new_password = validated_data.pop('password', '')
        if new_password:
            instance.set_password(new_password)
        return super().update(instance, validated_data)
