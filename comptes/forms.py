"""
    Formulaires de l'application Comptes
"""

from datetime import timedelta

from django import forms

from django.db import transaction

from django.utils import timezone
from django.core.exceptions import ValidationError
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.forms import PasswordChangeForm

from note_kfet.environnement import ACCREDITATIONS_DEFAUT

from comptes.models import (
    Adherent, Alias, Adhesion, Droit, Section, Historique, Accreditation,
)

### Formulaires concernant le modèle Adherent

class AdherentInscriptionForm(UserCreationForm):
    """
        Un formulaire pour l'inscription d'un nouvel adhérent.
    """
    section = forms.ModelChoiceField(
        required=False,
        queryset=Section.objects.filter(ferme=False),
    )

    class Meta:
        model = Adherent
        fields = [
            'type', 'nom', 'prenom', 'adresse', 'sexe',
            'email', 'telephone', 'section', 'pseudo', 'password1',
            'password2', 'remunere', 'pbsante',
        ]

    def clean_pseudo(self):
        """
            Vérifie que le pseudo demandé n'est pas déjà pris
        """
        pseudo = self._meta.model.normalize_username(self.cleaned_data['pseudo'])
        qs = Alias.objects.filter(alias=pseudo)
        if qs.filter(proprietaire__isnull=False).exists():
            raise ValidationError("Le pseudo %s est déjà pris." % self.cleaned_data['pseudo'])

        return pseudo

    def clean_section(self):
        """
            Vérifie que la section demandée n'est pas fermée
        """
        section = self.cleaned_data.get('section', None)
        if section and section.ferme:
            raise ValidationError("Cette section est fermée.")

        return section

    def clean(self):
        """
            Vérifie que le formulaire est complet relativement au type de compte.
        """
        cleaned_data = super().clean()
        if (cleaned_data['type'] != Adherent.DEBIT and not cleaned_data.get('nom', '')):
            self.add_error(
                'nom',
                ValidationError("Veuillez rensigner votre nom."),
            )
        if (cleaned_data['type'] == Adherent.PERSONNE and not cleaned_data.get('prenom', '')):
            self.add_error(
                'prenom',
                ValidationError("Veuillez rensigner votre prénom."),
            )
        return cleaned_data

    def save(self, *args, **kwargs):
        """
            Crée l'adhérent, l'alias allant avec le pseudo, l'historique
            associé, l'adhésion qui va avec et met en place les droits de
            base pour le type de compte demandé.

            Toute l'opération est effectuée de manière atomique dans
            la base de données.
        """
        self.cleaned_data.pop('password2')
        self.cleaned_data['password'] = self.cleaned_data.pop('password1')
        return self._meta.model.objects.create(**self.cleaned_data)

class AdherentPasswordChangeForm(PasswordChangeForm):
    """
        Formulaire de changement de mot de passe pour un adhérent
    """
    def __init__(self, instance=None, **kwargs):
        super().__init__(instance, **kwargs)

class AdherentDroitForm(forms.Form):
    """
        Formulaire pour l'attribution d'un nouveau droit.
    """
    codename = forms.CharField(disabled=True)
    droit = forms.ModelChoiceField(required=False, queryset=Droit.objects.none())
    meta = forms.BooleanField(required=False)

    def __init__(self, *args, **kwargs):
        droit = kwargs.pop('initial')
        content_type, codename = droit.content_type, droit.codename
        changed_by = kwargs.pop('changed_by')
        accred = kwargs.pop('accreds').filter(droit__codename=codename).first()
        initial = {'codename' : codename}
        if accred:
            initial.update({
                'droit' : accred.droit,
                'meta' : accred.meta,
            })
        kwargs.update({'initial' : initial})
        super().__init__(*args, **kwargs)
        self.content_type, self.codename = content_type, codename
        self.changed_by = changed_by
        self.accred = accred
        self.fields['droit'].queryset = Droit.objects.filter(codename=codename).order_by('niveau')

    def clean(self):
        """
            Vérifie que la nouvelle accéditation a été donnée
            conformément aux droits de l'utilisateur :
            -   L'activation de la mention 'méta' nécéssite un super-utilisateur
            -   Une accréditation ne peut être donnée par un utilisateur sans le méta-droit correspondant
            -   L'accréditation donnée ne peut pas être plus élevée que celle de l'utilisateur accréditant.
        """
        cleaned_data = super().clean()
        if cleaned_data.get('meta', False) and not self.changed_by.is_superuser:
            raise ValidationError("Vous ne pouvez pas mettre de mention 'méta' sur cette accréditation")
        if not self.changed_by.has_meta("%s.%s" % (self.content_type.app_label, self.codename), 0):
            raise ValidationError("Vous n'avez pas le métadroit correspondant")
        if not self.changed_by.is_superuser:
            # On vérifie que l'utilisateur n'essaie pas de mettre un niveau d'accréditation plus élevé que le sien
            changed_by_accred = Accreditation.objects.filter(droit__codename=self.codename, adherent=self.changed_by).first()
            if (not changed_by_attr) or (cleaned_data.get('droit').niveau > changed_by_accred.niveau):
                raise ValidationError("Vous ne pouvez pas accréditer plus que vous ne l'êtes")
        else:
            # Les superutilisateurs ne sont pas soumis aux restrictions sur les niveaux d'accréditation
            pass
        return cleaned_data

class AdherentSuppressionForm(forms.Form):
    """
        Un formulaire pour la suppression d'un adhérent.

        Demande si le compte doit être anonymisé.
    """
    shred = forms.BooleanField(label="Anonymiser le compte ?", required=False)
