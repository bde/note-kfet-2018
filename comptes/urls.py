"""
    Configuration des URLs de l'application Comptes
"""

from django.conf.urls import url, include
from django.views.generic.base import RedirectView

from django_filters.views import FilterView

from comptes.login import NotePasswordResetView, NotePasswordResetConfirmView
from comptes.models import Adherent
from comptes.views import (
    NoteLoginView, NoteLogoutView, NotePasswordResetDoneView, NotePasswordResetCompleteView,
    AdherentInscriptionView, AdherentDetailView, AdherentRechercheView,
    AdherentUpdateView, AdherentDroitChangeView, AdherentPasswordChangeView,
    AdherentAvatarView, AdherentDeleteView, AliasesHistoriqueView,
)


urlpatterns = [
    # Vues d'authentification
    url(r'^login/$', NoteLoginView.as_view(), name='login'),
    url(r'^logout/$', NoteLogoutView.as_view(), name='logout'),
    url(r'^password_reset/$', NotePasswordResetView.as_view(), name='password_reset'),
    url(r'^password_reset/done/$', NotePasswordResetDoneView.as_view(), name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', NotePasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    url(r'^reset/done/$', NotePasswordResetCompleteView.as_view(), name='password_reset_complete'),
    # Vues pour la gestion des comptes
    url(r'^$', RedirectView.as_view(permanent=True, pattern_name="comptes:recherche"), name="accueil"),
    url(r'^inscrire/$', AdherentInscriptionView.as_view(), name="inscription"),
    url(r'^(?P<pk>\d+)/$', AdherentDetailView.as_view(), name="detail"),
    url(r'^(?P<pk>\d+)/modifier/$', AdherentUpdateView.as_view(), name="modification"),
    url(r'^(?P<pk>\d+)/droits/$', AdherentDroitChangeView.as_view(), name="droits"),
    url(r'^(?P<pk>\d+)/avatar/$', AdherentAvatarView.as_view(), name="avatar"),
    url(r'^(?P<pk>\d+)/pw_change/$', AdherentPasswordChangeView.as_view(), name="pw_change"),
    url(r'^(?P<pk>\d+)/supprimer/$', AdherentDeleteView.as_view(), name="suppression"),
    url(r'^rechercher/$', AdherentRechercheView.as_view(), name="recherche"),
    # Vues pour la gestion des aliases
    url(r'^aliases/(?P<pk>\d+)/$', AliasesHistoriqueView.as_view(), name="aliases_historique"),
]
